<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<link rel="stylesheet" type="text/css" href="../../../system/css/global.css">
<link rel="stylesheet" type="text/css" href="../css/contents.css">
<link rel="stylesheet" type="text/css" href="../../../system/css/facebox.css">
<link rel="stylesheet" type="text/css" href="../css/print.css" id="print_css">
<script type="text/javascript" src="../../../system/js/util.js?cntl=Contents" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/dict_const.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/dict_message.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/use.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/contents.js" charset="UTF-8"></script>
<title>Toyota Service Information</title>
</head>
<body>
<div id="wrapper">
<div id="header"></div>
<div id="body">
<div id="contents">
<div id="contentsBody" class="fontEn">
<div class="globalInfo">
<span class="globalPubNo">RM2660E</span>
<span class="globalServcat">_51</span>
<span class="globalServcatName">Engine / Hybrid System</span>
<span class="globalSection">_052774</span>
<span class="globalSectionName">1GR-FE ENGINE CONTROL</span>
<span class="globalTitle">_0255001</span>
<span class="globalTitleName">SFI SYSTEM</span>
<span class="globalCategory">C</span>
</div>
<h1>1GR-FE ENGINE CONTROL&nbsp;&nbsp;SFI SYSTEM&nbsp;&nbsp;P2102&nbsp;&nbsp;Throttle Actuator Control Motor Circuit Low&nbsp;&nbsp;P2103&nbsp;&nbsp;Throttle Actuator Control Motor Circuit High&nbsp;&nbsp;</h1>
<br>
<div id="RM100000000UYO8_01" class="category no03">
<h2>DESCRIPTION</h2>
<div class="content5">
<p>The throttle actuator is operated by the ECM and opens and closes the throttle valve using gears.
</p>
<p>The opening angle of the throttle valve is detected by the throttle position sensor, which is mounted on the throttle body with motor assembly. The throttle position sensor provides feedback to the ECM. This feedback allows the ECM to appropriately control the throttle actuator and monitor the throttle opening angle as the ECM responds to driver inputs.
</p>
<dl class="atten4">
<dt class="atten4">HINT:</dt>
<dd class="atten4">
<p>This ETCS (Electronic Throttle Control System) does not use a throttle cable.
</p>
</dd>
<dd class="atten4"><table summary="">
<colgroup>
<col style="width:12%">
<col style="width:44%">
<col style="width:43%">
</colgroup>
<thead>
<tr>
<th class="alcenter">DTC No.
</th>
<th class="alcenter">DTC Detection Condition
</th>
<th class="alcenter">Trouble Area
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alcenter">P2102
</td>
<td>Conditions (a) and (b) are met for 2.0 seconds (1 trip detection logic):
<br>
(a) Throttle actuator duty ratio is 80% or more.
<br>
(b) Throttle actuator current is below 0.5 A.
</td>
<td><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Open in throttle actuator circuit
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Throttle actuator
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>ECM
</p>
</div>
</div>
</div>
<br>
</td>
</tr>
<tr>
<td class="alcenter">P2103
</td>
<td>Either condition is met (1 trip detection logic):
<br>
<div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>There is a hybrid IC diagnosis signal failure.
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>There is a hybrid IC current limiter port failure.
</p>
</div>
</div>
</div>
<br>
</td>
<td><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Short in throttle actuator circuit
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Throttle actuator
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Throttle valve
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Throttle body with motor assembly
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>ECM
</p>
</div>
</div>
</div>
<br>
</td>
</tr>
</tbody>
</table>
<br>
</dd>
</dl>
<br>
</div>
</div>
<div id="RM100000000UYO8_02" class="category no03">
<h2>MONITOR DESCRIPTION</h2>
<div class="content5">
<p>The ECM monitors the electrical current flowing through the electronic actuator and detects malfunctions and open circuits in the throttle actuator based on this value. If the current is outside the standard range, the ECM determines that there is a malfunction in the throttle actuator. In addition, if the throttle valve does not function properly (for example, if it is stuck on), the ECM determines that there is a malfunction. The ECM then illuminates the MIL and stores a DTC.
</p>
<p>Example:
</p>
<p>When the electrical current is below 0.5 A and the throttle actuator duty ratio is 80% or more, the ECM interprets this as the current being outside the standard range, illuminates the MIL and stores DTC P2102.
</p>
<p>If the malfunction is not repaired successfully, the DTC is stored when the engine is quickly revved to a high rpm several times after the engine is started and has idled for 5 seconds.
</p>
</div>
</div>
<div id="RM100000000UYO8_06" class="category no03">
<h2>FAIL-SAFE</h2>
<div class="content5">
<p>When either of these DTCs or other DTCs relating to ETCS (Electronic Throttle Control System) malfunctions are stored, the ECM enters fail-safe mode. During fail-safe mode, the ECM cuts the current to the throttle actuator, and the throttle valve is returned to a 7&deg; opening angle by the return spring. The ECM then adjusts the engine output by controlling the fuel injection (intermittent fuel-cut) and ignition timing in accordance with the accelerator pedal position to allow the vehicle to continue at a minimal speed. If the accelerator pedal is depressed firmly and gently, the vehicle can be driven slowly.
</p>
<p>The ECM continues operating in fail-safe mode until a pass condition is detected and the ignition switch is turned off.
</p>
</div>
</div>
<div id="RM100000000UYO8_07" class="category no03">
<h2>WIRING DIAGRAM</h2>
<div class="content5">
<div class="figure">
<div class="cPattern">
<div class="graphic">
<img src="../img/png/A162854E20.png" alt="A162854E20" title="A162854E20">
<div class="indicateinfo">
<span class="caption">
<span class="points">1.125,0.979 1.74,1.177</span>
<span class="captionSize">0.615,0.198</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">ETCS</span>
</span>
<span class="caption">
<span class="points">0.625,4.083 1.646,4.385</span>
<span class="captionSize">1.021,0.302</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">Battery</span>
</span>
<span class="caption">
<span class="points">2.5,1.396 2.781,1.604</span>
<span class="captionSize">0.281,0.208</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">M+</span>
</span>
<span class="caption">
<span class="points">2.521,2.219 2.802,2.427</span>
<span class="captionSize">0.281,0.208</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">M-</span>
</span>
<span class="caption">
<span class="points">1.917,2.74 2.333,2.969</span>
<span class="captionSize">0.417,0.229</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">C3</span>
</span>
<span class="caption">
<span class="points">1.917,2.896 3.031,3.281</span>
<span class="captionSize">1.115,0.385</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">Throttle Actuator</span>
</span>
<span class="caption">
<span class="points">2.802,1.365 3.083,1.573</span>
<span class="captionSize">0.281,0.208</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">2</span>
</span>
<span class="caption">
<span class="points">2.792,2.188 3.073,2.396</span>
<span class="captionSize">0.281,0.208</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">1</span>
</span>
<span class="caption">
<span class="points">4.708,0.719 4.938,0.896</span>
<span class="captionSize">0.229,0.177</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">41</span>
</span>
<span class="caption">
<span class="points">4.708,1.375 4.938,1.552</span>
<span class="captionSize">0.229,0.177</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">19</span>
</span>
<span class="caption">
<span class="points">4.688,2.208 4.917,2.385</span>
<span class="captionSize">0.229,0.177</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">18</span>
</span>
<span class="caption">
<span class="points">4.698,2.854 4.927,3.031</span>
<span class="captionSize">0.229,0.177</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">17</span>
</span>
<span class="caption">
<span class="points">4.688,3.396 4.917,3.573</span>
<span class="captionSize">0.229,0.177</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">81</span>
</span>
<span class="caption">
<span class="points">4.688,3.917 4.917,4.094</span>
<span class="captionSize">0.229,0.177</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">82</span>
</span>
<span class="caption">
<span class="points">3.906,2.688 4.542,3.094</span>
<span class="captionSize">0.635,0.406</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">Shielded</span>
</span>
<span class="caption">
<span class="points">4.979,0.719 5.354,0.896</span>
<span class="captionSize">0.375,0.177</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">+BM</span>
</span>
<span class="caption">
<span class="points">4.979,1.375 5.354,1.552</span>
<span class="captionSize">0.375,0.177</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">M+</span>
</span>
<span class="caption">
<span class="points">4.979,2.219 5.354,2.396</span>
<span class="captionSize">0.375,0.177</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">M-</span>
</span>
<span class="caption">
<span class="points">5,2.948 5.688,3.188</span>
<span class="captionSize">0.688,0.24</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">GE01</span>
</span>
<span class="caption">
<span class="points">5.01,3.5 5.698,3.74</span>
<span class="captionSize">0.688,0.24</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">E1</span>
</span>
<span class="caption">
<span class="points">5.01,3.979 5.698,4.219</span>
<span class="captionSize">0.688,0.24</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">ME01</span>
</span>
<span class="caption">
<span class="points">5.5,1.5 6.583,1.885</span>
<span class="captionSize">1.083,0.385</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">Throttle Actuator Control Circuit</span>
</span>
<span class="caption">
<span class="points">5.448,4.708 6.365,4.938</span>
<span class="captionSize">0.917,0.229</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">C45*1, C46*2</span>
</span>
<span class="caption">
<span class="points">5.448,4.875 6.365,5.104</span>
<span class="captionSize">0.917,0.229</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">ECM</span>
</span>
<span class="caption">
<span class="points">1.802,4.99 2.719,5.219</span>
<span class="captionSize">0.917,0.229</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">*1: for LHD</span>
</span>
<span class="caption">
<span class="points">1.802,5.292 2.719,5.521</span>
<span class="captionSize">0.917,0.229</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">*2: for RHD</span>
</span>
</div>
</div>
</div>
</div>
<br>
</div>
</div>
<div id="RM100000000UYO8_08" class="category no10">
<h2>CAUTION / NOTICE / HINT</h2>
<div class="content5">
<dl class="atten4">
<dt class="atten4">HINT:</dt>
<dd class="atten4"><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.
</p>
</div>
</div>
</div>
<br>
</dd>
<dd class="atten4"><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>The throttle actuator current (Throttle Motor Current) and the throttle actuator duty ratio (Throttle Motor Duty (Open) / Throttle Motor Duty (Close)) can be read using the GTS. However, the ECM shuts off the throttle actuator current when the ETCS malfunctions.
</p>
</div>
</div>
</div>
<br>
</dd>
</dl>
<br>
</div>
</div>
<div id="RM100000000UYO8_09" class="category no01">
<h2>PROCEDURE</h2>
<div class="testgrp" id="RM100000000UYO8_09_0001">
<div class="testtitle"><span class="titleText">1.INSPECT THROTTLE BODY WITH MOTOR ASSEMBLY (RESISTANCE OF THROTTLE ACTUATOR)</span></div>
<div class="content6">
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Inspect the throttle body with motor assembly (See page <a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;1GR-FE (ENGINE CONTROL)&gt;THROTTLE BODY&gt;INSPECTION<span class="invisible">201408,999999,_51,_052774,_0255012,RM100000000UYS0,</span></a>).
</p>
</div>
</div>
</div>
<br>
</div>
<br>
<div class="judgeValueNext"><p>OK
</p></div>
<div class="nextAction"><span class="titleText">2.CHECK HARNESS AND CONNECTOR (THROTTLE BODY WITH MOTOR ASSEMBLY - ECM)</span></div>
<div class="judgeValueEnd"><p>NG
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REPLACE THROTTLE BODY WITH MOTOR ASSEMBLY</span></div>
<div class="content6">
<p>(See page<a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;1GR-FE (ENGINE CONTROL)&gt;THROTTLE BODY&gt;REMOVAL<span class="invisible">201408,999999,_51,_052774,_0255012,RM100000000UYS2,</span></a>)
</p>
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="floatClear"></div>
</div>
<div class="testgrp" id="RM100000000UYO8_09_0002">
<div class="testtitle"><span class="titleText">2.CHECK HARNESS AND CONNECTOR (THROTTLE BODY WITH MOTOR ASSEMBLY - ECM)</span></div>
<div class="content6">
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Disconnect the throttle body with motor assembly connector.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">b.</div>
<div class="test1Body">
<p>Disconnect the ECM connector.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">c.</div>
<div class="test1Body">
<p>Measure the resistance according to the value(s) in the table below.
</p>
<dl class="spec">
<dt class="spec">Standard Resistance:</dt>
<dd class="spec"><table summary="" class="half">
<caption>for LHD</caption>
<colgroup>
<col style="width:33%">
<col style="width:33%">
<col style="width:33%">
</colgroup>
<thead>
<tr>
<th>Tester Connection
</th>
<th>Condition
</th>
<th>Specified Condition
</th>
</tr>
</thead>
<tbody>
<tr>
<td>C3-2 (M+) - C45-19 (M+)
</td>
<td>Always
</td>
<td>Below 1 Ω
</td>
</tr>
<tr>
<td>C3-1 (M-) - C45-18 (M-)
</td>
<td>Always
</td>
<td>Below 1 Ω
</td>
</tr>
<tr>
<td>C3-2 (M+) or C45-19 (M+) - Body ground
</td>
<td>Always
</td>
<td>10 kΩ or higher
</td>
</tr>
<tr>
<td>C3-1 (M-) or C45-18 (M-) - Body ground
</td>
<td>Always
</td>
<td>10 kΩ or higher
</td>
</tr>
</tbody>
</table>
<br>
</dd>
<dd class="spec"><table summary="" class="half">
<caption>for RHD</caption>
<colgroup>
<col style="width:33%">
<col style="width:33%">
<col style="width:33%">
</colgroup>
<thead>
<tr>
<th>Tester Connection
</th>
<th>Condition
</th>
<th>Specified Condition
</th>
</tr>
</thead>
<tbody>
<tr>
<td>C3-2 (M+) - C46-19 (M+)
</td>
<td>Always
</td>
<td>Below 1 Ω
</td>
</tr>
<tr>
<td>C3-1 (M-) - C46-18 (M-)
</td>
<td>Always
</td>
<td>Below 1 Ω
</td>
</tr>
<tr>
<td>C3-2 (M+) or C46-19 (M+) - Body ground
</td>
<td>Always
</td>
<td>10 kΩ or higher
</td>
</tr>
<tr>
<td>C3-1 (M-) or C46-18 (M-) - Body ground
</td>
<td>Always
</td>
<td>10 kΩ or higher
</td>
</tr>
</tbody>
</table>
<br>
</dd>
</dl>
<br>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">d.</div>
<div class="test1Body">
<p>Reconnect the throttle body with motor assembly connector.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">e.</div>
<div class="test1Body">
<p>Reconnect the ECM connector.
</p>
</div>
</div>
</div>
<br>
</div>
<br>
<div class="judgeValueNext"><p>OK
</p></div>
<div class="nextAction"><span class="titleText">3.INSPECT THROTTLE BODY WITH MOTOR ASSEMBLY</span></div>
<div class="judgeValueEnd"><p>NG
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REPAIR OR REPLACE HARNESS OR CONNECTOR</span></div>
<div class="content6">
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="floatClear"></div>
</div>
<div class="testgrp" id="RM100000000UYO8_09_0003">
<div class="testtitle"><span class="titleText">3.INSPECT THROTTLE BODY WITH MOTOR ASSEMBLY</span></div>
<div class="content6">
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Check for foreign objects between the throttle valve and housing.
</p>
<dl class="spec">
<dt class="spec">OK:</dt>
<dd class="spec"><p>No foreign objects between throttle valve and housing.
</p>
</dd>
</dl>
<br>
</div>
</div>
</div>
<br>
</div>
<br>
<div class="judgeValueNext"><p>OK
</p></div>
<div class="nextAction"><span class="titleText">4.INSPECT THROTTLE VALVE</span></div>
<div class="judgeValueEnd"><p>NG
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REMOVE FOREIGN OBJECT AND CLEAN THROTTLE BODY WITH MOTOR ASSEMBLY</span></div>
<div class="content6">
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="floatClear"></div>
</div>
<div class="testgrp" id="RM100000000UYO8_09_0004">
<div class="testtitle"><span class="titleText">4.INSPECT THROTTLE VALVE</span></div>
<div class="content6">
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Check if the throttle valve opens and closes smoothly.
</p>
<dl class="spec">
<dt class="spec">OK:</dt>
<dd class="spec"><p>Throttle valve opens and closes smoothly.
</p>
</dd>
</dl>
<br>
</div>
</div>
</div>
<br>
</div>
<br>
<div class="judgeValueEnd"><p>OK
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REPLACE ECM</span></div>
<div class="content6">
<p>(See page<a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;1GR-FE (ENGINE CONTROL)&gt;ECM&gt;REMOVAL<span class="invisible">201408,201508,_51,_052774,_0255013,RM100000000UYS8,</span><span class="invisible">201508,999999,_51,_052774,_0255013,RM100000000UYS9,</span></a>)
</p>
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="judgeValueEnd"><p>NG
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REPLACE THROTTLE BODY WITH MOTOR ASSEMBLY</span></div>
<div class="content6">
<p>(See page<a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;1GR-FE (ENGINE CONTROL)&gt;THROTTLE BODY&gt;REMOVAL<span class="invisible">201408,999999,_51,_052774,_0255012,RM100000000UYS2,</span></a>)
</p>
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="floatClear"></div>
</div>
</div>
</div>
</div>
<div id="footer"></div>
</div>
</div>
</body>
</html>
