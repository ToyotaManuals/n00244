<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE tmc-service-paragraph SYSTEM "../dtd/paragraph-contents.dtd">
<tmc-service-paragraph><pub id="RM2660E"><servcat id="_58"><name>Vehicle Interior</name><section id="_052847"><name>HEATING / AIR CONDITIONING</name><ttl id="_0255699"><name>AIR CONDITIONING SYSTEM(for Automatic Air Conditioning System)</name><para id="RM100000000V3QZ" category="C"><dtccode>B1422/22</dtccode><dtcname>Compressor Lock Sensor Circuit</dtcname><subpara id="RM100000000V3QZ_01" category="03"><name>SYSTEM DESCRIPTION</name><content5><ptxt>The ECM sends the engine speed signal to the air conditioning amplifier assembly via CAN communication.</ptxt><ptxt>The air conditioning amplifier assembly reads the difference between compressor speed and engine speed. When the difference becomes too large, the air conditioning amplifier assembly determines that the compressor is locked, and turns the magnetic clutch off.</ptxt><table pgwide="1"><tgroup align="center" cols="3"><colspec colname="COL1" colwidth="1.42in"/><colspec colname="COL2" colwidth="2.83in"/><colspec colname="COL3" colwidth="2.83in"/><thead><row><entry valign="middle" align="center"><ptxt>DTC Code</ptxt></entry><entry valign="middle" align="center"><ptxt>DTC Detection Condition</ptxt></entry><entry valign="middle" align="center"><ptxt>Trouble Area</ptxt></entry></row></thead><tbody><row><entry valign="middle" align="center"><ptxt>B1422/22</ptxt></entry><entry align="left" valign="middle"><ptxt>An open or short in the compressor lock sensor circuit.</ptxt></entry><entry align="left" valign="middle"><list1 type="unordered"><item><ptxt>CAN communication line</ptxt></item><item><ptxt>Cooler compressor assembly</ptxt></item><item><ptxt>Harness or connector</ptxt></item><item><ptxt>Air conditioning amplifier assembly</ptxt></item></list1></entry></row></tbody></tgroup></table></content5></subpara><subpara id="RM100000000V3QZ_02" category="03"><name>WIRING DIAGRAM</name><content5><step1><ptxt>w/ Rear Heater:</ptxt><figure type="1" translate="E"><graphic graphicname="E334401E05" width="7.168479331in" height="5.838041339in"/></figure></step1><step1><ptxt>w/o Rear Heater:</ptxt><figure translate="E" type="1"><graphic graphicname="E334419E01" height="5.838041339in" width="7.168479331in"/></figure></step1></content5></subpara><subpara id="RM100000000V3QZ_03" category="10"><name>CAUTION / NOTICE / HINT</name><content5><atten3><list1 type="unordered"><item><ptxt>ECM malfunctions can affect the storage of this DTC. Therefore, check all ECM DTCs and confirm that the system is normal before performing the following inspection.</ptxt></item><list2 type="nonmark"><item><ptxt>for 1GR-FE: See page <mlink><xref label="Seep00001" term-id-from="001" href="RM100000000WNAS"/></mlink></ptxt></item><item><ptxt>for 1VD-FTV (w/ DPF): See page <mlink><xref label="Seep00002" term-id-from="001" href="RM100000000WNNQ"/></mlink></ptxt></item><item><ptxt>for 1VD-FTV (w/o DPF): See page <mlink><xref label="Seep00003" term-id-from="001" href="RM100000000WNO4"/></mlink></ptxt></item><item><ptxt>for 1UR-FE: See page <mlink><xref label="Seep00004" term-id-from="000" href="RM100000000WN0J"/></mlink></ptxt></item><item><ptxt>for 3UR-FE: See page <mlink><xref label="Seep00005" term-id-from="000" href="RM100000000WN0O"/></mlink></ptxt></item></list2><item><ptxt>The air conditioning system uses the CAN communication system. Inspect the communication function by following How to Proceed with Troubleshooting (See page <mlink><xref label="Seep00006" term-id-from="001" href="RM100000000WOXB"/></mlink>).</ptxt></item></list1></atten3></content5></subpara><subpara id="RM100000000V3QZ_06" category="01"><name>PROCEDURE</name><testgrp id="RM100000000V3QZ_06_0003"><testtitle>INSPECT MAGNET CLUTCH ASSEMBLY</testtitle><content6><test1><ptxt>Remove the magnet clutch assembly.</ptxt><list1 type="nonmark"><item><ptxt>for 1GR-FE: See page <mlink><xref label="Seep00001" term-id-from="000" href="RM100000000V3NZ"/></mlink></ptxt></item><item><ptxt>for 1VD-FTV: See page <mlink><xref label="Seep00002" term-id-from="000" href="RM100000000V3O5"/></mlink></ptxt></item><item><ptxt>for 1UR-FE: See page <mlink><xref label="Seep00003" term-id-from="000" href="RM100000000V3VN"/></mlink></ptxt></item><item><ptxt>for 3UR-FE: See page <mlink><xref label="Seep00004" term-id-from="000" href="RM100000000V3TP"/></mlink></ptxt></item></list1></test1><test1><ptxt>Inspect the magnet clutch assembly.</ptxt><list1 type="nonmark"><item><ptxt>for 1GR-FE: See page <mlink><xref label="Seep00005" term-id-from="000" href="RM100000000V3NW"/></mlink></ptxt></item><item><ptxt>for 1VD-FTV: See page <mlink><xref label="Seep00006" term-id-from="000" href="RM100000000V3O2"/></mlink></ptxt></item><item><ptxt>for 1UR-FE: See page <mlink><xref label="Seep00007" term-id-from="000" href="RM100000000V3VK"/></mlink></ptxt></item><item><ptxt>for 3UR-FE: See page <mlink><xref label="Seep00008" term-id-from="000" href="RM100000000V3TM"/></mlink></ptxt></item></list1><table><title>Result</title><tgroup cols="2"><colspec colname="COL1" colwidth="2.89in"/><colspec colname="COL2" colwidth="1.24in"/><thead><row><entry align="center" valign="middle"><ptxt>Result</ptxt></entry><entry align="center" valign="middle"><ptxt>Proceed to</ptxt></entry></row></thead><tbody><row><entry align="center" valign="middle"><ptxt>OK</ptxt></entry><entry align="center" valign="middle"><ptxt>A</ptxt></entry></row><row><entry align="center" valign="middle"><ptxt>NG (for 1GR-FE)</ptxt></entry><entry align="center" valign="middle"><ptxt>B</ptxt></entry></row><row><entry align="center" valign="middle"><ptxt>NG (for 1VD-FTV)</ptxt></entry><entry align="center" valign="middle"><ptxt>C</ptxt></entry></row><row><entry align="center" valign="middle"><ptxt>NG (for 1UR-FE)</ptxt></entry><entry align="center" valign="middle"><ptxt>D</ptxt></entry></row><row><entry align="center" valign="middle"><ptxt>NG (for 3UR-FE)</ptxt></entry><entry align="center" valign="middle"><ptxt>E</ptxt></entry></row></tbody></tgroup></table></test1></content6><results><result label="A"><action actiontype="next" childid="RM100000000V3QZ_06_0002"/></result><result label="B"><action actiontype="end"><actiontitle>REPLACE MAGNET CLUTCH ASSEMBLY</actiontitle><content6><ptxt>(See page<mlink><xref label="Seep00001" term-id-from="000" href="RM100000000V3NZ"/></mlink>)</ptxt></content6></action></result><result label="C"><action actiontype="end"><actiontitle>REPLACE MAGNET CLUTCH ASSEMBLY</actiontitle><content6><ptxt>(See page<mlink><xref label="Seep00001" term-id-from="000" href="RM100000000V3O5"/></mlink>)</ptxt></content6></action></result><result label="D"><action actiontype="end"><actiontitle>REPLACE MAGNET CLUTCH ASSEMBLY</actiontitle><content6><ptxt>(See page<mlink><xref label="Seep00001" term-id-from="000" href="RM100000000V3VN"/></mlink>)</ptxt></content6></action></result><result label="E"><action actiontype="end"><actiontitle>REPLACE MAGNET CLUTCH ASSEMBLY</actiontitle><content6><ptxt>(See page<mlink><xref label="Seep00001" term-id-from="000" href="RM100000000V3TP"/></mlink>)</ptxt></content6></action></result></results></testgrp><testgrp id="RM100000000V3QZ_06_0002"><testtitle>CHECK AIR CONDITIONING AMPLIFIER ASSEMBLY</testtitle><content6><test1><ptxt>w/ Rear Heater:</ptxt><test2><ptxt>Remove the air conditioning amplifier assembly with its connectors still connected (See page <mlink><xref label="Seep00001" term-id-from="001" href="RM100000000V3MX"/></mlink>).</ptxt><figure type="1" translate="E"><graphic graphicname="U156837E04" width="7.168479331in" height="1.787155512in"/></figure><table pgwide="1"><title>Text in Illustration</title><tgroup align="left" cols="4"><colspec colname="COL1" colwidth="0.71in"/><colspec colname="COL2" colwidth="2.83in"/><colspec colname="COL3" colwidth="0.71in"/><colspec align="left" colname="COL4" colwidth="2.83in"/><tbody><row><entry align="center" valign="middle"><ptxt>*a</ptxt></entry><entry valign="middle"><ptxt>Component with harness connected</ptxt><ptxt>((Air Conditioning Amplifier Assembly)</ptxt></entry><entry align="center" valign="middle"><ptxt>*b</ptxt></entry><entry valign="middle"><ptxt>Waveform</ptxt></entry></row></tbody></tgroup></table></test2><test2><ptxt>Measure the waveform of the connector.</ptxt><table><title>Measurement Condition</title><tgroup cols="2"><colspec colname="COL1" colwidth="1.24in"/><colspec colname="COL2" colwidth="2.89in"/><thead><row><entry align="center" valign="middle"><ptxt>Item</ptxt></entry><entry align="center" valign="middle"><ptxt>Contents</ptxt></entry></row></thead><tbody><row><entry align="center" valign="middle"><ptxt>Terminal No. (Symbol)</ptxt></entry><entry align="center" valign="middle"><ptxt>E35-30 (LOCK) - E35-32 (SG-2)</ptxt></entry></row><row><entry align="center" valign="middle"><ptxt>Tool Setting</ptxt></entry><entry align="center" valign="middle"><ptxt>200 mV/DIV., 10 ms/DIV.</ptxt></entry></row><row><entry align="center" valign="middle"><ptxt>Condition</ptxt></entry><entry valign="middle"><list1 type="unordered"><item><ptxt>Engine idling</ptxt></item><item><ptxt>A/C switch on</ptxt></item><item><ptxt>Blower switch LO</ptxt></item></list1></entry></row></tbody></tgroup></table></test2></test1><test1><ptxt>w/o Rear Heater:</ptxt><figure translate="E" type="1"><graphic graphicname="U166594E01" height="1.787155512in" width="7.168479331in"/></figure><table pgwide="1"><title>Text in Illustration</title><tgroup align="left" cols="4"><colspec colname="COL1" colwidth="0.71in"/><colspec colname="COL2" colwidth="2.83in"/><colspec colname="COL3" colwidth="0.71in"/><colspec align="left" colname="COL4" colwidth="2.83in"/><tbody><row><entry align="center" valign="middle"><ptxt>*a</ptxt></entry><entry valign="middle"><ptxt>Component with harness connected</ptxt><ptxt>((Air Conditioning Amplifier Assembly)</ptxt></entry><entry align="center" valign="middle"><ptxt>*b</ptxt></entry><entry valign="middle"><ptxt>Waveform</ptxt></entry></row></tbody></tgroup></table><test2><ptxt>Remove the air conditioning amplifier assembly with its connectors still connected (See page <mlink><xref label="Seep00003" term-id-from="001" href="RM100000000V3MX"/></mlink>).</ptxt></test2><test2><ptxt>Measure the waveform of the connector.</ptxt><table><title>Measurement Condition</title><tgroup cols="2"><colspec colname="COL1" colwidth="1.24in"/><colspec colname="COL2" colwidth="2.89in"/><thead><row><entry align="center" valign="middle"><ptxt>Item</ptxt></entry><entry align="center" valign="middle"><ptxt>Contents</ptxt></entry></row></thead><tbody><row><entry align="center" valign="middle"><ptxt>Terminal No. (Symbol)</ptxt></entry><entry align="center" valign="middle"><ptxt>E81-8 (LOCK) - E81-13 (SG-2)</ptxt></entry></row><row><entry align="center" valign="middle"><ptxt>Tool Setting</ptxt></entry><entry align="center" valign="middle"><ptxt>200 mV/DIV., 10 ms/DIV.</ptxt></entry></row><row><entry align="center" valign="middle"><ptxt>Condition</ptxt></entry><entry align="center" valign="middle"><ptxt>Engine running</ptxt><ptxt>A/C switch on</ptxt><ptxt>Blower switch LO</ptxt></entry></row></tbody></tgroup></table></test2></test1></content6><results><result label="OK"><action actiontype="end"><actiontitle>REPLACE AIR CONDITIONING AMPLIFIER ASSEMBLY</actiontitle><content6><ptxt>(See page<mlink><xref label="Seep00001" term-id-from="001" href="RM100000000V3MX"/></mlink>)</ptxt></content6></action></result><result label="NG"><action actiontype="next" childid="RM100000000V3QZ_06_0014"/></result></results></testgrp><testgrp id="RM100000000V3QZ_06_0014"><testtitle>INSPECT COOLER COMPRESSOR ASSEMBLY</testtitle><content6><test1><ptxt>Remove the cooler compressor assembly.</ptxt><list1 type="nonmark"><item><ptxt>for 1GR-FE: See page <mlink><xref label="Seep00001" term-id-from="000" href="RM100000000V3NY"/></mlink></ptxt></item><item><ptxt>for 1VD-FTV: See page <mlink><xref label="Seep00002" term-id-from="000" href="RM100000000V3O4"/></mlink></ptxt></item><item><ptxt>for 1UR-FE: See page <mlink><xref label="Seep00003" term-id-from="000" href="RM100000000V3VM"/></mlink></ptxt></item><item><ptxt>for 3UR-FE: See page <mlink><xref label="Seep00004" term-id-from="000" href="RM100000000V3TO"/></mlink></ptxt></item></list1></test1><test1><ptxt>Inspect the cooler compressor assembly.</ptxt><list1 type="nonmark"><item><ptxt>for 1GR-FE: See page <mlink><xref label="Seep00005" term-id-from="000" href="RM100000000V3NW"/></mlink></ptxt></item><item><ptxt>for 1VD-FTV: See page <mlink><xref label="Seep00006" term-id-from="000" href="RM100000000V3O2"/></mlink></ptxt></item><item><ptxt>for 1UR-FE: See page <mlink><xref label="Seep00007" term-id-from="000" href="RM100000000V3VK"/></mlink></ptxt></item><item><ptxt>for 3UR-FE: See page <mlink><xref label="Seep00008" term-id-from="000" href="RM100000000V3TM"/></mlink></ptxt></item></list1><table><title>Result</title><tgroup cols="2"><colspec colname="COL1" colwidth="2.89in"/><colspec colname="COL2" colwidth="1.24in"/><thead><row><entry align="center" valign="middle"><ptxt>Result</ptxt></entry><entry align="center" valign="middle"><ptxt>Proceed to</ptxt></entry></row></thead><tbody><row><entry align="center" valign="middle"><ptxt>OK</ptxt></entry><entry align="center" valign="middle"><ptxt>A</ptxt></entry></row><row><entry align="center" valign="middle"><ptxt>NG (for 1GR-FE)</ptxt></entry><entry align="center" valign="middle"><ptxt>B</ptxt></entry></row><row><entry align="center" valign="middle"><ptxt>NG (for 1VD-FTV)</ptxt></entry><entry align="center" valign="middle"><ptxt>C</ptxt></entry></row><row><entry align="center" valign="middle"><ptxt>NG (for 1UR-FE)</ptxt></entry><entry align="center" valign="middle"><ptxt>D</ptxt></entry></row><row><entry align="center" valign="middle"><ptxt>NG (for 3UR-FE)</ptxt></entry><entry align="center" valign="middle"><ptxt>E</ptxt></entry></row></tbody></tgroup></table></test1></content6><results><result label="A"><action actiontype="next" childid="RM100000000V3QZ_06_0004"/></result><result label="B"><action actiontype="end"><actiontitle>REPLACE COOLER COMPRESSOR ASSEMBLY</actiontitle><content6><ptxt>(See page<mlink><xref label="Seep00001" term-id-from="000" href="RM100000000V3NY"/></mlink>)</ptxt></content6></action></result><result label="C"><action actiontype="end"><actiontitle>REPLACE COOLER COMPRESSOR ASSEMBLY</actiontitle><content6><ptxt>(See page<mlink><xref label="Seep00001" term-id-from="000" href="RM100000000V3O4"/></mlink>)</ptxt></content6></action></result><result label="D"><action actiontype="end"><actiontitle>REPLACE COOLER COMPRESSOR ASSEMBLY</actiontitle><content6><ptxt>(See page<mlink><xref label="Seep00001" term-id-from="000" href="RM100000000V3VM"/></mlink>)</ptxt></content6></action></result><result label="E"><action actiontype="end"><actiontitle>REPLACE COOLER COMPRESSOR ASSEMBLY</actiontitle><content6><ptxt>(See page<mlink><xref label="Seep00001" term-id-from="000" href="RM100000000V3TM"/></mlink>)</ptxt></content6></action></result></results></testgrp><testgrp id="RM100000000V3QZ_06_0004"><testtitle>CHECK HARNESS AND CONNECTOR (AIR CONDITIONING AMPLIFIER ASSEMBLY -COOLER COMPRESSOR ASSEMBLY)</testtitle><content6><test1><ptxt>w/ Rear Heater:</ptxt><test2><ptxt>Disconnect the E35 air conditioning amplifier assembly connector.</ptxt></test2><test2><ptxt>Disconnect the C30 cooler compressor assembly connector.</ptxt></test2><test2><ptxt>Measure the resistance according to the value(s) in the table below.</ptxt><spec type="resistance"><title>Standard Resistance</title><table><tgroup align="center" cols="3"><colspec colname="COL1" colwidth="1.38in"/><colspec colname="COL2" colwidth="1.38in"/><colspec colname="COL3" colwidth="1.37in"/><thead><row><entry valign="middle" align="center"><ptxt>Tester Connection</ptxt></entry><entry valign="middle" align="center"><ptxt>Condition</ptxt></entry><entry valign="middle" align="center"><ptxt>Specified Condition</ptxt></entry></row></thead><tbody><row><entry valign="middle" align="center"><ptxt>E35-30 (LOCK) - C30-1 (SSR+)</ptxt></entry><entry valign="middle" align="center"><ptxt>Always</ptxt></entry><entry valign="middle" align="center"><ptxt>Below 1 Ω</ptxt></entry></row><row><entry valign="middle" align="center"><ptxt>E35-32 (SG-2) - C30-2 (SSR-)</ptxt></entry><entry valign="middle" align="center"><ptxt>Always</ptxt></entry><entry valign="middle" align="center"><ptxt>Below 1 Ω</ptxt></entry></row><row><entry valign="middle" align="center"><ptxt>C30-1 (SSR+) - C30-2 (SSR-)</ptxt></entry><entry valign="middle" align="center"><ptxt>Always</ptxt></entry><entry valign="middle" align="center"><ptxt>10 kΩ or higher</ptxt></entry></row><row><entry valign="middle" align="center"><ptxt>E35-30 (LOCK) or C30-1 (SSR+) - Body ground</ptxt></entry><entry valign="middle" align="center"><ptxt>Always</ptxt></entry><entry valign="middle" align="center"><ptxt>10 kΩ or higher</ptxt></entry></row><row><entry valign="middle" align="center"><ptxt>E35-32 (SG-2) or C30-2 (SSR-) - Body ground</ptxt></entry><entry valign="middle" align="center"><ptxt>Always</ptxt></entry><entry valign="middle" align="center"><ptxt>10 kΩ or higher</ptxt></entry></row></tbody></tgroup></table></spec></test2></test1><test1><ptxt>w/o Rear Heater:</ptxt><test2><ptxt>Disconnect the E81 air conditioning amplifier assembly connector.</ptxt></test2><test2><ptxt>Disconnect the C30 cooler compressor assembly connector.</ptxt></test2><test2><ptxt>Measure the resistance according to the value(s) in the table below.</ptxt><spec type="resistance"><title>Standard Resistance</title><table><tgroup align="center" cols="3"><colspec colname="COL1" colwidth="1.38in"/><colspec colname="COL2" colwidth="1.38in"/><colspec colname="COL3" colwidth="1.37in"/><thead><row><entry valign="middle" align="center"><ptxt>Tester Connection</ptxt></entry><entry valign="middle" align="center"><ptxt>Condition</ptxt></entry><entry valign="middle" align="center"><ptxt>Specified Condition</ptxt></entry></row></thead><tbody><row><entry valign="middle" align="center"><ptxt>E81-8 (LOCK) - C30-1 (SSR+)</ptxt></entry><entry valign="middle" align="center"><ptxt>Always</ptxt></entry><entry valign="middle" align="center"><ptxt>Below 1 Ω</ptxt></entry></row><row><entry valign="middle" align="center"><ptxt>E81-13 (SG-2) - C30-2 (SSR-)</ptxt></entry><entry valign="middle" align="center"><ptxt>Always</ptxt></entry><entry valign="middle" align="center"><ptxt>Below 1 Ω</ptxt></entry></row><row><entry valign="middle" align="center"><ptxt>C30-1 (SSR+) - C30-2 (SSR-)</ptxt></entry><entry valign="middle" align="center"><ptxt>Always</ptxt></entry><entry valign="middle" align="center"><ptxt>Below 1 Ω</ptxt></entry></row><row><entry valign="middle" align="center"><ptxt>E81-8 (LOCK) or C30-1 (SSR+) - Body ground</ptxt></entry><entry valign="middle" align="center"><ptxt>Always</ptxt></entry><entry valign="middle" align="center"><ptxt>10 kΩ or higher</ptxt></entry></row><row><entry valign="middle" align="center"><ptxt>E81-13 (SG-2) or C30-2 (SSR-) - Body ground</ptxt></entry><entry valign="middle" align="center"><ptxt>Always</ptxt></entry><entry valign="middle" align="center"><ptxt>10 kΩ or higher</ptxt></entry></row></tbody></tgroup></table></spec></test2></test1></content6><results><result label="OK"><action actiontype="end"><actiontitle>REPLACE AIR CONDITIONING AMPLIFIER ASSEMBLY</actiontitle><content6><ptxt>(See page<mlink><xref label="Seep00001" term-id-from="001" href="RM100000000V3MX"/></mlink>)</ptxt></content6></action></result><result label="NG"><action actiontype="end"><actiontitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</actiontitle><content6/></action></result></results></testgrp></subpara></para></ttl></section></servcat></pub></tmc-service-paragraph>