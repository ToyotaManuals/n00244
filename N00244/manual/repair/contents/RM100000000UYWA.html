<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<link rel="stylesheet" type="text/css" href="../../../system/css/global.css">
<link rel="stylesheet" type="text/css" href="../css/contents.css">
<link rel="stylesheet" type="text/css" href="../../../system/css/facebox.css">
<link rel="stylesheet" type="text/css" href="../css/print.css" id="print_css">
<script type="text/javascript" src="../../../system/js/util.js?cntl=Contents" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/dict_const.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/dict_message.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/use.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/contents.js" charset="UTF-8"></script>
<title>Toyota Service Information</title>
</head>
<body>
<div id="wrapper">
<div id="header"></div>
<div id="body">
<div id="contents">
<div id="contentsBody" class="fontEn">
<div class="globalInfo">
<span class="globalPubNo">RM2660E</span>
<span class="globalServcat">_51</span>
<span class="globalServcatName">Engine / Hybrid System</span>
<span class="globalSection">_052775</span>
<span class="globalSectionName">3UR-FE ENGINE CONTROL</span>
<span class="globalTitle">_0255032</span>
<span class="globalTitleName">SFI SYSTEM</span>
<span class="globalCategory">C</span>
</div>
<h1>3UR-FE ENGINE CONTROL&nbsp;&nbsp;SFI SYSTEM&nbsp;&nbsp;P2102&nbsp;&nbsp;Throttle Actuator Control Motor Circuit Low&nbsp;&nbsp;P2103&nbsp;&nbsp;Throttle Actuator Control Motor Circuit High&nbsp;&nbsp;</h1>
<br>
<div id="RM100000000UYWA_01" class="category no03">
<h2>DESCRIPTION</h2>
<div class="content5">
<p>The throttle actuator is operated by the ECM and opens and closes the throttle valve using gears.
</p>
<p>The opening angle of the throttle valve is detected by the Throttle Position (TP) sensor, which is mounted on the throttle body. The TP sensor provides feedback to the ECM. This feedback allows the ECM to appropriately control the throttle actuator and monitor the throttle opening angle as the ECM responds to driver inputs.
</p>
<dl class="atten4">
<dt class="atten4">HINT:</dt>
<dd class="atten4">
<p>This ETCS (Electronic Throttle Control System) does not use a throttle cable.
</p>
</dd>
<dd class="atten4"><table summary="">
<colgroup>
<col style="width:12%">
<col style="width:44%">
<col style="width:43%">
</colgroup>
<thead>
<tr>
<th class="alcenter">DTC Code
</th>
<th class="alcenter">DTC Detection Condition
</th>
<th class="alcenter">Trouble Area
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alcenter">P2102
</td>
<td>Conditions (a) and (b) continue for 2.0 seconds (1 trip detection logic):
<br>
(a) The throttle actuator duty ratio is 80% or more.
<br>
(b) The throttle actuator current is below 0.5 A.
</td>
<td><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Open in throttle actuator circuit
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Throttle actuator
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>ECM
</p>
</div>
</div>
</div>
<br>
</td>
</tr>
<tr>
<td class="alcenter">P2103
</td>
<td>Either of the following conditions is met (1 trip detection logic):
<br>
<div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>A hybrid IC diagnosis signal failure.
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>A hybrid IC current limiter port failure.
</p>
</div>
</div>
</div>
<br>
</td>
<td><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Short in throttle actuator circuit
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Throttle actuator
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Throttle valve
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Throttle body assembly
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>ECM
</p>
</div>
</div>
</div>
<br>
</td>
</tr>
</tbody>
</table>
<br>
</dd>
</dl>
<br>
</div>
</div>
<div id="RM100000000UYWA_02" class="category no03">
<h2>MONITOR DESCRIPTION</h2>
<div class="content5">
<p>The ECM monitors the electrical current through the electronic actuator, and detects malfunctions and open circuits in the throttle actuator based on this value. If the current is outside the standard range, the ECM determines that there is a malfunction in the throttle actuator. In addition, if the throttle valve does not function properly (for example, stuck on), the ECM determines that there is a malfunction. The ECM then illuminates the MIL and stores DTC(s).
</p>
<p>Example:
</p>
<p>When the electrical current is below 0.5 A and the throttle actuator duty ratio exceeds 80%, the ECM interprets this as the current being outside the standard range, illuminates the MIL and stores DTC(s).
</p>
<p>If the malfunction is not repaired successfully, a DTC is stored when the engine is quickly revved to a high rpm several times after the engine is started and has idled for 5 seconds.
</p>
</div>
</div>
<div id="RM100000000UYWA_06" class="category no03">
<h2>FAIL-SAFE</h2>
<div class="content5">
<p>When either of these DTCs, or other DTCs relating to ETCS (Electronic Throttle Control System) malfunctions are stored, the ECM enters fail-safe mode. During fail-safe mode, the ECM cuts the current to the throttle actuator, and the throttle valve is returned to a 7&deg; throttle angle by the return spring. The ECM then adjusts the engine output by controlling the fuel injection (intermittent fuel cut) and ignition timing, in accordance with the accelerator pedal opening angle, to allow the vehicle to continue at a minimal speed. If the accelerator pedal is depressed firmly and gently, the vehicle can be driven slowly.
</p>
<p>Fail-safe mode continues until a pass condition is detected, and the engine switch is then turned off.
</p>
</div>
</div>
<div id="RM100000000UYWA_07" class="category no03">
<h2>WIRING DIAGRAM</h2>
<div class="content5">
<div class="figure">
<div class="cPattern">
<div class="graphic">
<img src="../img/png/A162853E09.png" alt="A162853E09" title="A162853E09">
<div class="indicateinfo">
<span class="caption">
<span class="points">5.576,3.144 5.912,3.300</span>
<span class="captionSize">0.336,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">ECM</span>
</span>
<span class="caption">
<span class="points">5.398,0.944 6.461,1.313</span>
<span class="captionSize">1.063,0.369</span>
<span class="fontsize">10</span>
<span class="captionText">Throttle Actuator Control Circuit</span>
</span>
<span class="caption">
<span class="points">4.884,0.87 5.124,1.044</span>
<span class="captionSize">0.239,0.174</span>
<span class="fontsize">10</span>
<span class="captionText">M+</span>
</span>
<span class="caption">
<span class="points">4.884,1.622 5.073,1.782</span>
<span class="captionSize">0.188,0.160</span>
<span class="fontsize">10</span>
<span class="captionText">M-</span>
</span>
<span class="caption">
<span class="points">1.182,0.866 1.421,1.040</span>
<span class="captionSize">0.239,0.174</span>
<span class="fontsize">10</span>
<span class="captionText">M+</span>
</span>
<span class="caption">
<span class="points">1.206,1.617 1.394,1.778</span>
<span class="captionSize">0.188,0.160</span>
<span class="fontsize">10</span>
<span class="captionText">M-</span>
</span>
<span class="caption">
<span class="points">5.578,3.005 5.908,3.161</span>
<span class="captionSize">0.330,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">C45</span>
</span>
<span class="caption">
<span class="points">4.668,0.87 4.861,1.026</span>
<span class="captionSize">0.193,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">19</span>
</span>
<span class="caption">
<span class="points">4.668,1.618 4.861,1.774</span>
<span class="captionSize">0.193,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">18</span>
</span>
<span class="caption">
<span class="points">4.668,2.388 4.861,2.544</span>
<span class="captionSize">0.193,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">17</span>
</span>
<span class="caption">
<span class="points">2.709,2.247 3.282,2.413</span>
<span class="captionSize">0.573,0.165</span>
<span class="fontsize">10</span>
<span class="captionText">Shielded</span>
</span>
<span class="caption">
<span class="points">0.273,2.316 1.331,2.486</span>
<span class="captionSize">1.059,0.170</span>
<span class="fontsize">10</span>
<span class="captionText">Throttle Actuator</span>
</span>
<span class="caption">
<span class="points">0.273,2.158 0.484,2.316</span>
<span class="captionSize">0.211,0.158</span>
<span class="fontsize">10</span>
<span class="captionText">C3</span>
</span>
<span class="caption">
<span class="points">1.452,0.867 1.559,1.037</span>
<span class="captionSize">0.107,0.170</span>
<span class="fontsize">10</span>
<span class="captionText">2</span>
</span>
<span class="caption">
<span class="points">1.44,1.614 1.547,1.784</span>
<span class="captionSize">0.107,0.170</span>
<span class="fontsize">10</span>
<span class="captionText">1</span>
</span>
<span class="caption">
<span class="points">4.902,2.465 5.294,2.641</span>
<span class="captionSize">0.392,0.177</span>
<span class="fontsize">10</span>
<span class="captionText">GE01</span>
</span>
</div>
</div>
</div>
</div>
<br>
</div>
</div>
<div id="RM100000000UYWA_08" class="category no10">
<h2>CAUTION / NOTICE / HINT</h2>
<div class="content5">
<dl class="atten4">
<dt class="atten4">HINT:</dt>
<dd class="atten4"><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.
</p>
</div>
</div>
</div>
<br>
</dd>
<dd class="atten4"><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>The throttle actuator current (Throttle Motor Current) and the throttle actuator duty ratio (Throttle Motor Open Duty / Throttle Motor Close Duty) can be read using the intelligent tester. However, the ECM shuts off the throttle actuator current when the ETCS malfunctions.
</p>
</div>
</div>
</div>
<br>
</dd>
</dl>
<br>
</div>
</div>
<div id="RM100000000UYWA_09" class="category no01">
<h2>PROCEDURE</h2>
<div class="testgrp" id="RM100000000UYWA_09_0001">
<div class="testtitle"><span class="titleText">1.INSPECT THROTTLE BODY ASSEMBLY (RESISTANCE OF THROTTLE ACTUATOR)</span></div>
<div class="content6">
<div class="contentPartition"></div>
<div class="figureAPatternGroup">
<div class="figure">
<div class="aPattern">
<div class="graphic">
<img src="../img/png/A150305E03.png" alt="A150305E03" title="A150305E03">
<div class="indicateinfo">
<span class="caption">
<span class="points">0.074,0.196 2.612,0.588</span>
<span class="captionSize">2.538,0.392</span>
<span class="fontsize">10</span>
<span class="captionText">Component without harness connected: (Throttle Body)</span>
</span>
<span class="caption">
<span class="points">1.931,2.455 2.208,2.629</span>
<span class="captionSize">0.277,0.173</span>
<span class="fontsize">10</span>
<span class="captionText">M+</span>
</span>
<span class="caption">
<span class="points">2.563,2.455 2.841,2.629</span>
<span class="captionSize">0.277,0.173</span>
<span class="fontsize">10</span>
<span class="captionText">M-</span>
</span>
<span class="line">
<span class="points">2.24,1.946 2.103,2.401</span>
<span class="whiteedge">false</span>
</span>
<span class="line">
<span class="points">2.532,1.937 2.61,2.401</span>
<span class="whiteedge">false</span>
</span>
</div>
</div>
</div>
</div>
<br>
</div>
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Disconnect the C3 throttle body connector.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">b.</div>
<div class="test1Body">
<p>Measure the resistance according to the value(s) in the table below.
</p>
<dl class="spec">
<dt class="spec">Standard Resistance:</dt>
<dd class="spec"><table summary="" class="half">
<colgroup>
<col style="width:33%">
<col style="width:33%">
<col style="width:33%">
</colgroup>
<thead>
<tr>
<th>Tester Connection
</th>
<th>Condition
</th>
<th>Specified Condition
</th>
</tr>
</thead>
<tbody>
<tr>
<td>2 (M+) - 1 (M-)
</td>
<td>20&deg;C (68&deg;F)
</td>
<td>0.3 to 100 Ω
</td>
</tr>
</tbody>
</table>
<br>
</dd>
</dl>
<br>
</div>
</div>
</div>
<br>
</div>
<br>
<div class="judgeValueNext"><p>OK
</p></div>
<div class="nextAction"><span class="titleText">2.CHECK HARNESS AND CONNECTOR (THROTTLE ACTUATOR - ECM)</span></div>
<div class="judgeValueEnd"><p>NG
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REPLACE THROTTLE BODY ASSEMBLY</span></div>
<div class="content6">
<p>(See page<a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;3UR-FE (ENGINE CONTROL)&gt;THROTTLE BODY&gt;REMOVAL<span class="invisible">201408,999999,_51,_052775,_0255019,RM100000000UYSZ,</span></a>)
</p>
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="floatClear"></div>
</div>
<div class="testgrp" id="RM100000000UYWA_09_0002">
<div class="testtitle"><span class="titleText">2.CHECK HARNESS AND CONNECTOR (THROTTLE ACTUATOR - ECM)</span></div>
<div class="content6">
<div class="contentPartition"></div>
<div class="figureAPatternGroup">
<div class="figure">
<div class="aPattern">
<div class="graphic">
<img src="../img/png/A385734E01.png" alt="A385734E01" title="A385734E01">
<div class="indicateinfo">
<span class="line">
<span class="points">1.542,1.281 1.219,1.021</span>
<span class="whiteedge">true</span>
</span>
<span class="line">
<span class="points">1.51,1.656 1.698,1.385</span>
<span class="whiteedge">true</span>
</span>
<span class="line">
<span class="points">2,3.396 1.688,4.313</span>
<span class="whiteedge">true</span>
</span>
<span class="line">
<span class="points">2.094,3.385 2.396,4.292</span>
<span class="whiteedge">true</span>
</span>
<span class="caption">
<span class="points">0.385,0.063 2.771,0.656</span>
<span class="captionSize">2.385,0.594</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">Front view of wire harness connector: (to Throttle Body)</span>
</span>
<span class="caption">
<span class="points">0.844,1.229 1.208,1.5</span>
<span class="captionSize">0.365,0.271</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">C3</span>
</span>
<span class="caption">
<span class="points">1.042,0.938 1.406,1.208</span>
<span class="captionSize">0.365,0.271</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">M-</span>
</span>
<span class="caption">
<span class="points">1.313,1.615 1.677,1.885</span>
<span class="captionSize">0.365,0.271</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">M+</span>
</span>
<span class="caption">
<span class="points">0.406,1.885 2.792,2.479</span>
<span class="captionSize">2.385,0.594</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">Front view of wire harness connector: (to ECM)</span>
</span>
<span class="caption">
<span class="points">1.583,2.781 1.948,3.052</span>
<span class="captionSize">0.365,0.271</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">C45</span>
</span>
<span class="caption">
<span class="points">1.417,4.323 1.906,4.563</span>
<span class="captionSize">0.49,0.24</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">18 (M-)</span>
</span>
<span class="caption">
<span class="points">2.146,4.323 2.635,4.563</span>
<span class="captionSize">0.49,0.24</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">19 (M+)</span>
</span>
</div>
</div>
</div>
</div>
<br>
</div>
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Disconnect the C3 throttle body connector.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">b.</div>
<div class="test1Body">
<p>Disconnect the C45 ECM connector.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">c.</div>
<div class="test1Body">
<p>Measure the resistance according to the value(s) in the table below.
</p>
<dl class="spec">
<dt class="spec">Standard Resistance:</dt>
<dd class="spec"><table summary="" class="half">
<colgroup>
<col style="width:33%">
<col style="width:33%">
<col style="width:33%">
</colgroup>
<thead>
<tr>
<th>Tester Connection
</th>
<th>Condition
</th>
<th>Specified Condition
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alleft">C3-2 (M+) - C45-19 (M+)
</td>
<td>Always
</td>
<td>Below 1 Ω
</td>
</tr>
<tr>
<td class="alleft">C3-1 (M-) - C45-18 (M-)
</td>
<td>Always
</td>
<td>Below 1 Ω
</td>
</tr>
<tr>
<td class="alleft">C3-2 (M+) or C45-19 (M+) - Body ground
</td>
<td>Always
</td>
<td>10 kΩ or higher
</td>
</tr>
<tr>
<td class="alleft">C3-1 (M-) or C45-18 (M-) - Body ground
</td>
<td>Always
</td>
<td>10 kΩ or higher
</td>
</tr>
</tbody>
</table>
<br>
</dd>
</dl>
<br>
</div>
</div>
</div>
<br>
</div>
<br>
<div class="judgeValueNext"><p>OK
</p></div>
<div class="nextAction"><span class="titleText">3.INSPECT THROTTLE BODY ASSEMBLY</span></div>
<div class="judgeValueEnd"><p>NG
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REPAIR OR REPLACE HARNESS OR CONNECTOR</span></div>
<div class="content6">
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="floatClear"></div>
</div>
<div class="testgrp" id="RM100000000UYWA_09_0003">
<div class="testtitle"><span class="titleText">3.INSPECT THROTTLE BODY ASSEMBLY</span></div>
<div class="content6">
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Check for foreign objects between the throttle valve and the housing.
</p>
<dl class="spec">
<dt class="spec">OK:</dt>
<dd class="spec"><p>No foreign objects between throttle valve and housing.
</p>
</dd>
</dl>
<br>
</div>
</div>
</div>
<br>
</div>
<br>
<div class="judgeValueNext"><p>OK
</p></div>
<div class="nextAction"><span class="titleText">4.INSPECT THROTTLE VALVE</span></div>
<div class="judgeValueEnd"><p>NG
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REMOVE FOREIGN OBJECT AND CLEAN THROTTLE BODY</span></div>
<div class="content6">
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="floatClear"></div>
</div>
<div class="testgrp" id="RM100000000UYWA_09_0004">
<div class="testtitle"><span class="titleText">4.INSPECT THROTTLE VALVE</span></div>
<div class="content6">
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Check if the throttle valve opens and closes smoothly.
</p>
<dl class="spec">
<dt class="spec">OK:</dt>
<dd class="spec"><p>Throttle valve opens and closes smoothly.
</p>
</dd>
</dl>
<br>
</div>
</div>
</div>
<br>
</div>
<br>
<div class="judgeValueEnd"><p>OK
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REPLACE ECM</span></div>
<div class="content6">
<p>(See page<a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;3UR-FE (ENGINE CONTROL)&gt;ECM&gt;REMOVAL<span class="invisible">201408,201508,_51,_052775,_0255020,RM100000000UYT5,</span></a>)
</p>
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="judgeValueEnd"><p>NG
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REPLACE THROTTLE BODY ASSEMBLY</span></div>
<div class="content6">
<p>(See page<a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;3UR-FE (ENGINE CONTROL)&gt;THROTTLE BODY&gt;REMOVAL<span class="invisible">201408,999999,_51,_052775,_0255019,RM100000000UYSZ,</span></a>)
</p>
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="floatClear"></div>
</div>
</div>
</div>
</div>
<div id="footer"></div>
</div>
</div>
</body>
</html>
