<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<link rel="stylesheet" type="text/css" href="../../../system/css/global.css">
<link rel="stylesheet" type="text/css" href="../css/contents.css">
<link rel="stylesheet" type="text/css" href="../../../system/css/facebox.css">
<link rel="stylesheet" type="text/css" href="../css/print.css" id="print_css">
<script type="text/javascript" src="../../../system/js/util.js?cntl=Contents" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/dict_const.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/dict_message.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/use.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/contents.js" charset="UTF-8"></script>
<title>Toyota Service Information</title>
</head>
<body>
<div id="wrapper">
<div id="header"></div>
<div id="body">
<div id="contents">
<div id="contentsBody" class="fontEn">
<div class="globalInfo">
<span class="globalPubNo">RM2660E</span>
<span class="globalServcat">_51</span>
<span class="globalServcatName">Engine / Hybrid System</span>
<span class="globalSection">_052772</span>
<span class="globalSectionName">1UR-FE ENGINE CONTROL</span>
<span class="globalTitle">_0254983</span>
<span class="globalTitleName">SFI SYSTEM</span>
<span class="globalCategory">C</span>
</div>
<h1>1UR-FE ENGINE CONTROL&nbsp;&nbsp;SFI SYSTEM&nbsp;&nbsp;P2102&nbsp;&nbsp;Throttle Actuator Control Motor Circuit Low&nbsp;&nbsp;P2103&nbsp;&nbsp;Throttle Actuator Control Motor Circuit High&nbsp;&nbsp;</h1>
<br>
<div id="RM100000000UY88_01" class="category no03">
<h2>DESCRIPTION</h2>
<div class="content5">
<p>The throttle actuator is operated by the ECM and opens and closes the throttle valve using gears.
</p>
<p>The opening angle of the throttle valve is detected by the throttle position sensor, which is mounted on the throttle body. The throttle position sensor provides feedback to the ECM. This feedback allows the ECM to appropriately control the throttle actuator and monitor the throttle opening angle as the ECM responds to driver inputs.
</p>
<dl class="atten4">
<dt class="atten4">HINT:</dt>
<dd class="atten4">
<p>This ETCS (Electronic Throttle Control System) does not use a throttle cable.
</p>
</dd>
<dd class="atten4"><table summary="">
<colgroup>
<col style="width:12%">
<col style="width:44%">
<col style="width:43%">
</colgroup>
<thead>
<tr>
<th class="alcenter">DTC No.
</th>
<th class="alcenter">DTC Detection Condition
</th>
<th class="alcenter">Trouble Area
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alcenter">P2102
</td>
<td>Conditions (a) and (b) continue for 2.0 seconds (1 trip detection logic):
<br>
(a) Throttle actuator duty ratio is 80% or more.
<br>
(b) Throttle actuator current is below 0.5 A.
</td>
<td><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Open in throttle actuator circuit
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Throttle actuator
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>ECM
</p>
</div>
</div>
</div>
<br>
</td>
</tr>
<tr>
<td class="alcenter">P2103
</td>
<td>Either condition is met (1 trip detection logic):
<br>
<div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>There is a hybrid IC diagnosis signal failure.
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>There is a hybrid IC current limiter port failure.
</p>
</div>
</div>
</div>
<br>
</td>
<td><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Short in throttle actuator circuit
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Throttle actuator
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Throttle valve
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Throttle body assembly
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>ECM
</p>
</div>
</div>
</div>
<br>
</td>
</tr>
</tbody>
</table>
<br>
</dd>
</dl>
<br>
</div>
</div>
<div id="RM100000000UY88_02" class="category no03">
<h2>MONITOR DESCRIPTION</h2>
<div class="content5">
<p>The ECM monitors the electrical current through the electronic actuator, and detects malfunctions and open circuits in the throttle actuator based on this value. If the current is outside the standard range, the ECM determines that there is a malfunction in the throttle actuator. In addition, if the throttle valve does not function properly (for example, if it is stuck on), the ECM determines that there is a malfunction. The ECM then illuminates the MIL and stores a DTC.
</p>
<p>Example:
</p>
<p>When the electrical current is less than 0.5 A and the throttle actuator duty ratio is 80% or more, the ECM interprets this as the current being outside the standard range, illuminates the MIL and stores a DTC.
</p>
<p>If the malfunction is not repaired successfully, a DTC is stored when the engine is quickly revved to a high engine speed several times after the engine is started and has idled for 5 seconds.
</p>
</div>
</div>
<div id="RM100000000UY88_10" class="category no03">
<h2>CONFIRMATION DRIVING PATTERN</h2>
<div class="content5">
<div class="figure">
<div class="cPattern">
<div class="graphic">
<img src="../img/png/A199313E12.png" alt="A199313E12" title="A199313E12">
<div class="indicateinfo">
<span class="caption">
<span class="points">1.246,1.847 1.624,2.017</span>
<span class="captionSize">0.378,0.170</span>
<span class="fontsize">10</span>
<span class="captionText">Idling</span>
</span>
<span class="caption">
<span class="points">0.478,2.118 1.591,2.503</span>
<span class="captionSize">1.113,0.385</span>
<span class="fontsize">10</span>
<span class="captionText">Engine Switch on (IG)</span>
</span>
<span class="caption">
<span class="points">2.125,1.985 2.315,2.172</span>
<span class="captionSize">0.191,0.187</span>
<span class="fontsize">10</span>
<span class="captionText">[A]</span>
</span>
<span class="caption">
<span class="points">3.855,1.742 4.041,1.929</span>
<span class="captionSize">0.187,0.187</span>
<span class="fontsize">10</span>
<span class="captionText">[B]</span>
</span>
<span class="caption">
<span class="points">5.49,0.986 6.125,1.134</span>
<span class="captionSize">0.635,0.149</span>
<span class="fontsize">10</span>
<span class="captionText">Released</span>
</span>
<span class="caption">
<span class="points">3.589,0.292 4.321,0.441</span>
<span class="captionSize">0.732,0.149</span>
<span class="fontsize">10</span>
<span class="captionText">Depressed</span>
</span>
<span class="caption">
<span class="points">3.488,2.718 4.684,2.902</span>
<span class="captionSize">1.195,0.184</span>
<span class="fontsize">10</span>
<span class="captionText">2 seconds or more</span>
</span>
<span class="caption">
<span class="points">5.256,3.288 5.493,3.492</span>
<span class="captionSize">0.237,0.204</span>
<span class="fontsize">10</span>
<span class="captionText">[C]</span>
</span>
<span class="caption">
<span class="points">3.629,3.017 5.240,3.201</span>
<span class="captionSize">1.612,0.184</span>
<span class="fontsize">10</span>
<span class="captionText">16 seconds or more</span>
</span>
<span class="line">
<span class="points">3.659,1.44 3.234,0.387</span>
<span class="points">3.234,0.387 3.521,0.387</span>
<span class="whiteedge">false</span>
</span>
<span class="line">
<span class="points">4.824,1.936 5.251,1.074</span>
<span class="points">5.251,1.074 5.406,1.074</span>
<span class="whiteedge">false</span>
</span>
<span class="caption">
<span class="points">2.666,1.07 3.302,1.219</span>
<span class="captionSize">0.635,0.149</span>
<span class="fontsize">10</span>
<span class="captionText">Released</span>
</span>
<span class="line">
<span class="points">2.66,1.936 2.449,1.151</span>
<span class="points">2.449,1.151 2.611,1.151</span>
<span class="whiteedge">false</span>
</span>
</div>
</div>
</div>
</div>
<br>
<div class="list1">
<div class="list1Item">
<div class="list1Head">1.</div>
<div class="list1Body"><p>Connect the GTS to the DLC3.
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">2.</div>
<div class="list1Body"><p>Turn the engine switch on (IG) and turn the GTS on.
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">3.</div>
<div class="list1Body"><p>Clear DTCs (even if no DTCs are stored, perform the clear DTC operation).
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">4.</div>
<div class="list1Body"><p>Turn the engine switch off and wait for at least 30 seconds.
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">5.</div>
<div class="list1Body"><p>Turn the engine switch on (IG) and turn the GTS on [A].
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">6.</div>
<div class="list1Body"><p>Start the engine.
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">7.</div>
<div class="list1Body"><p>With the vehicle stationary, fully depress the accelerator pedal and quickly release it [B].
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">8.</div>
<div class="list1Body"><p>Check that 16 seconds or more have elapsed from the instant when the accelerator pedal is first depressed.
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">9.</div>
<div class="list1Body"><p>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes [C].
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">10.</div>
<div class="list1Body"><p>Read the pending DTCs.
</p>
<dl class="atten4">
<dt class="atten4">HINT:</dt>
<dd class="atten4"><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>If a pending DTC is output, the system is malfunctioning.
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>If a pending DTC is not output, perform the following procedure.
</p>
</div>
</div>
</div>
<br>
</dd>
</dl>
<br>
</div>
</div>
<div class="list1Item">
<div class="list1Head">11.</div>
<div class="list1Body"><p>Enter the following menus: Powertrain / Engine and ECT / Utility / All Readiness.
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">12.</div>
<div class="list1Body"><p>Input the DTC: P2102 or P2103.
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">13.</div>
<div class="list1Body"><p>Check the DTC judgment result.
</p>
<table summary="">
<colgroup>
<col style="width:29%">
<col style="width:70%">
</colgroup>
<thead>
<tr>
<th class="alcenter">GTS Display
</th>
<th class="alcenter">Description
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alcenter">NORMAL
</td>
<td><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>DTC judgment completed
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>System normal
</p>
</div>
</div>
</div>
<br>
</td>
</tr>
<tr>
<td class="alcenter">ABNORMAL
</td>
<td><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>DTC judgment completed
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>System abnormal
</p>
</div>
</div>
</div>
<br>
</td>
</tr>
<tr>
<td class="alcenter">INCOMPLETE
</td>
<td><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>DTC judgment not completed
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Perform driving pattern after confirming DTC enabling conditions
</p>
</div>
</div>
</div>
<br>
</td>
</tr>
<tr>
<td class="alcenter">N/A
</td>
<td><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Unable to perform DTC judgment
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Number of DTCs which do not fulfill DTC preconditions has reached ECU memory limit
</p>
</div>
</div>
</div>
<br>
</td>
</tr>
</tbody>
</table>
<br>
<dl class="atten4">
<dt class="atten4">HINT:</dt>
<dd class="atten4"><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>If the judgment result shows NORMAL, the system is normal.
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>If the judgment result shows ABNORMAL, the system has a malfunction.
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>If the judgment result shows INCOMPLETE or N/A, perform steps [B] through [C] again and, if necessary, drive the vehicle for a period of time.
</p>
</div>
</div>
</div>
<br>
</dd>
</dl>
<br>
</div>
</div>
</div>
<br>
</div>
</div>
<div id="RM100000000UY88_06" class="category no03">
<h2>FAIL-SAFE</h2>
<div class="content5">
<p>When either of these DTCs, or other DTCs relating to ETCS (Electronic Throttle Control System) malfunctions are stored, the ECM enters fail-safe mode. During fail-safe mode, the ECM cuts the current to the throttle actuator and the throttle valve is returned to a 7&deg; throttle angle by the return spring. The ECM then adjusts the engine output by controlling the fuel injection (intermittent fuel-cut) and ignition timing in accordance with the accelerator pedal position to allow the vehicle to continue at a minimal speed. If the accelerator pedal is depressed firmly and gently, the vehicle can be driven slowly.
</p>
<p>The ECM continues operating in fail-safe mode until a pass condition is detected, and the engine switch is then turned off.
</p>
</div>
</div>
<div id="RM100000000UY88_07" class="category no03">
<h2>WIRING DIAGRAM</h2>
<div class="content5">
<div class="figure">
<div class="cPattern">
<div class="graphic">
<img src="../img/png/A278624E03.png" alt="A278624E03" title="A278624E03">
<div class="indicateinfo">
<span class="caption">
<span class="points">4.971,3.207 5.307,3.363</span>
<span class="captionSize">0.336,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">ECM</span>
</span>
<span class="caption">
<span class="points">5.394,0.921 6.457,1.290</span>
<span class="captionSize">1.063,0.369</span>
<span class="fontsize">10</span>
<span class="captionText">Throttle Actuator Control Circuit</span>
</span>
<span class="caption">
<span class="points">4.88,0.846 5.120,1.021</span>
<span class="captionSize">0.239,0.174</span>
<span class="fontsize">10</span>
<span class="captionText">M+</span>
</span>
<span class="caption">
<span class="points">4.88,1.598 5.069,1.759</span>
<span class="captionSize">0.188,0.160</span>
<span class="fontsize">10</span>
<span class="captionText">M-</span>
</span>
<span class="caption">
<span class="points">1.178,0.842 1.417,1.016</span>
<span class="captionSize">0.239,0.174</span>
<span class="fontsize">10</span>
<span class="captionText">M+</span>
</span>
<span class="caption">
<span class="points">1.202,1.594 1.390,1.754</span>
<span class="captionSize">0.188,0.160</span>
<span class="fontsize">10</span>
<span class="captionText">M-</span>
</span>
<span class="caption">
<span class="points">4.636,0.846 4.829,1.002</span>
<span class="captionSize">0.193,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">19</span>
</span>
<span class="caption">
<span class="points">4.636,1.598 4.829,1.754</span>
<span class="captionSize">0.193,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">18</span>
</span>
<span class="caption">
<span class="points">4.636,2.351 4.829,2.506</span>
<span class="captionSize">0.193,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">17</span>
</span>
<span class="caption">
<span class="points">2.705,2.224 3.278,2.389</span>
<span class="captionSize">0.573,0.165</span>
<span class="fontsize">10</span>
<span class="captionText">Shielded</span>
</span>
<span class="caption">
<span class="points">0.269,2.349 1.327,2.518</span>
<span class="captionSize">1.059,0.170</span>
<span class="fontsize">10</span>
<span class="captionText">Throttle Actuator</span>
</span>
<span class="caption">
<span class="points">0.269,2.191 0.480,2.348</span>
<span class="captionSize">0.211,0.158</span>
<span class="fontsize">10</span>
<span class="captionText">C3</span>
</span>
<span class="caption">
<span class="points">1.448,0.844 1.555,1.014</span>
<span class="captionSize">0.107,0.170</span>
<span class="fontsize">10</span>
<span class="captionText">2</span>
</span>
<span class="caption">
<span class="points">1.436,1.59 1.543,1.760</span>
<span class="captionSize">0.107,0.170</span>
<span class="fontsize">10</span>
<span class="captionText">1</span>
</span>
<span class="caption">
<span class="points">4.898,2.441 5.290,2.618</span>
<span class="captionSize">0.392,0.177</span>
<span class="fontsize">10</span>
<span class="captionText">GE01</span>
</span>
<span class="caption">
<span class="points">4.971,3.038 5.913,3.193</span>
<span class="captionSize">0.942,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">C46*1, C45*2</span>
</span>
<span class="caption">
<span class="points">1.926,3.205 2.831,3.370</span>
<span class="captionSize">0.906,0.165</span>
<span class="fontsize">10</span>
<span class="captionText">*1: for RHD</span>
</span>
<span class="caption">
<span class="points">1.926,3.401 2.831,3.566</span>
<span class="captionSize">0.906,0.165</span>
<span class="fontsize">10</span>
<span class="captionText">*2: for LHD</span>
</span>
</div>
</div>
</div>
</div>
<br>
</div>
</div>
<div id="RM100000000UY88_08" class="category no10">
<h2>CAUTION / NOTICE / HINT</h2>
<div class="content5">
<dl class="atten4">
<dt class="atten4">HINT:</dt>
<dd class="atten4"><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.
</p>
</div>
</div>
</div>
<br>
</dd>
<dd class="atten4"><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>The throttle actuator current (Throttle Motor Current) and the throttle actuator duty ratio (Throttle Motor Open Duty/Throttle Motor Close Duty) can be read using the GTS. However, the ECM shuts off the throttle actuator current when the ETCS malfunctions.
</p>
</div>
</div>
</div>
<br>
</dd>
</dl>
<br>
</div>
</div>
<div id="RM100000000UY88_09" class="category no01">
<h2>PROCEDURE</h2>
<div class="testgrp" id="RM100000000UY88_09_0001">
<div class="testtitle"><span class="titleText">1.INSPECT THROTTLE BODY ASSEMBLY (RESISTANCE OF THROTTLE ACTUATOR)</span></div>
<div class="content6">
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Inspect the throttle body assembly (See page <a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;1UR-FE (ENGINE CONTROL)&gt;THROTTLE BODY&gt;INSPECTION<span class="invisible">201408,999999,_51,_052772,_0254969,RM100000000UY4O,</span></a>).
</p>
</div>
</div>
</div>
<br>
</div>
<br>
<div class="judgeValueNext"><p>OK
</p></div>
<div class="nextAction"><span class="titleText">2.CHECK HARNESS AND CONNECTOR (THROTTLE ACTUATOR - ECM)</span></div>
<div class="judgeValueEnd"><p>NG
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REPLACE THROTTLE BODY ASSEMBLY</span></div>
<div class="content6">
<p>(See page<a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;1UR-FE (ENGINE CONTROL)&gt;THROTTLE BODY&gt;REMOVAL<span class="invisible">201408,999999,_51,_052772,_0254969,RM100000000UY4Q,</span></a>)
</p>
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="floatClear"></div>
</div>
<div class="testgrp" id="RM100000000UY88_09_0002">
<div class="testtitle"><span class="titleText">2.CHECK HARNESS AND CONNECTOR (THROTTLE ACTUATOR - ECM)</span></div>
<div class="content6">
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Disconnect the throttle body connector.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">b.</div>
<div class="test1Body">
<p>Disconnect the ECM connector.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">c.</div>
<div class="test1Body">
<p>Measure the resistance according to the value(s) in the table below.
</p>
<dl class="spec">
<dt class="spec">Standard Resistance:</dt>
<dd class="spec"><table summary="">
<caption>for RHD</caption>
<colgroup>
<col style="width:50%">
<col style="width:25%">
<col style="width:25%">
</colgroup>
<thead>
<tr>
<th class="alcenter">Tester Connection
</th>
<th class="alcenter">Condition
</th>
<th class="alcenter">Specified Condition
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alcenter">C3-2 (M+) - C46-19 (M+)
</td>
<td class="alcenter">Always
</td>
<td class="alcenter">Below 1 Ω
</td>
</tr>
<tr>
<td class="alcenter">C3-1 (M-) - C46-18 (M-)
</td>
<td class="alcenter">Always
</td>
<td class="alcenter">Below 1 Ω
</td>
</tr>
<tr>
<td class="alcenter">C3-2 (M+) or C46-19 (M+) - Body ground
</td>
<td class="alcenter">Always
</td>
<td class="alcenter">10 kΩ or higher
</td>
</tr>
<tr>
<td class="alcenter">C3-1 (M-) or C46-18 (M-) - Body ground
</td>
<td class="alcenter">Always
</td>
<td class="alcenter">10 kΩ or higher
</td>
</tr>
</tbody>
</table>
<br>
</dd>
<dd class="spec"><table summary="">
<caption>for LHD</caption>
<colgroup>
<col style="width:50%">
<col style="width:25%">
<col style="width:25%">
</colgroup>
<thead>
<tr>
<th class="alcenter">Tester Connection
</th>
<th class="alcenter">Condition
</th>
<th class="alcenter">Specified Condition
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alcenter">C3-2 (M+) - C45-19 (M+)
</td>
<td class="alcenter">Always
</td>
<td class="alcenter">Below 1 Ω
</td>
</tr>
<tr>
<td class="alcenter">C3-1 (M-) - C45-18 (M-)
</td>
<td class="alcenter">Always
</td>
<td class="alcenter">Below 1 Ω
</td>
</tr>
<tr>
<td class="alcenter">C3-2 (M+) or C45-19 (M+) - Body ground
</td>
<td class="alcenter">Always
</td>
<td class="alcenter">10 kΩ or higher
</td>
</tr>
<tr>
<td class="alcenter">C3-1 (M-) or C45-18 (M-) - Body ground
</td>
<td class="alcenter">Always
</td>
<td class="alcenter">10 kΩ or higher
</td>
</tr>
</tbody>
</table>
<br>
</dd>
</dl>
<br>
</div>
</div>
</div>
<br>
</div>
<br>
<div class="judgeValueNext"><p>OK
</p></div>
<div class="nextAction"><span class="titleText">3.INSPECT THROTTLE BODY ASSEMBLY</span></div>
<div class="judgeValueEnd"><p>NG
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REPAIR OR REPLACE HARNESS OR CONNECTOR</span></div>
<div class="content6">
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="floatClear"></div>
</div>
<div class="testgrp" id="RM100000000UY88_09_0003">
<div class="testtitle"><span class="titleText">3.INSPECT THROTTLE BODY ASSEMBLY</span></div>
<div class="content6">
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Check for foreign objects between the throttle valve and the housing.
</p>
<dl class="spec">
<dt class="spec">OK:</dt>
<dd class="spec"><p>No foreign objects between throttle valve and housing.
</p>
</dd>
</dl>
<br>
</div>
</div>
</div>
<br>
</div>
<br>
<div class="judgeValueNext"><p>OK
</p></div>
<div class="nextAction"><span class="titleText">4.INSPECT THROTTLE VALVE</span></div>
<div class="judgeValueEnd"><p>NG
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REMOVE FOREIGN OBJECT AND CLEAN THROTTLE BODY</span></div>
<div class="content6">
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="floatClear"></div>
</div>
<div class="testgrp" id="RM100000000UY88_09_0004">
<div class="testtitle"><span class="titleText">4.INSPECT THROTTLE VALVE</span></div>
<div class="content6">
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Check if the throttle valve opens and closes smoothly.
</p>
<dl class="spec">
<dt class="spec">OK:</dt>
<dd class="spec"><p>Throttle valve opens and closes smoothly.
</p>
</dd>
</dl>
<br>
</div>
</div>
</div>
<br>
</div>
<br>
<div class="judgeValueEnd"><p>OK
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REPLACE ECM</span></div>
<div class="content6">
<p>(See page<a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;1UR-FE (ENGINE CONTROL)&gt;ECM&gt;REMOVAL<span class="invisible">201408,201508,_51,_052772,_0254970,RM100000000UY4W,</span></a>)
</p>
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="judgeValueEnd"><p>NG
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REPLACE THROTTLE BODY ASSEMBLY</span></div>
<div class="content6">
<p>(See page<a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;1UR-FE (ENGINE CONTROL)&gt;THROTTLE BODY&gt;REMOVAL<span class="invisible">201408,999999,_51,_052772,_0254969,RM100000000UY4Q,</span></a>)
</p>
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="floatClear"></div>
</div>
</div>
</div>
</div>
<div id="footer"></div>
</div>
</div>
</body>
</html>
