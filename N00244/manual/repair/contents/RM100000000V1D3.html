<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<link rel="stylesheet" type="text/css" href="../../../system/css/global.css">
<link rel="stylesheet" type="text/css" href="../css/contents.css">
<link rel="stylesheet" type="text/css" href="../../../system/css/facebox.css">
<link rel="stylesheet" type="text/css" href="../css/print.css" id="print_css">
<script type="text/javascript" src="../../../system/js/util.js?cntl=Contents" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/dict_const.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/dict_message.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/use.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/contents.js" charset="UTF-8"></script>
<title>Toyota Service Information</title>
</head>
<body>
<div id="wrapper">
<div id="header"></div>
<div id="body">
<div id="contents">
<div id="contentsBody" class="fontEn">
<div class="globalInfo">
<span class="globalPubNo">RM2660E</span>
<span class="globalServcat">_55</span>
<span class="globalServcatName">Steering</span>
<span class="globalSection">_052825</span>
<span class="globalSectionName">VARIABLE GEAR RATIO STEERING</span>
<span class="globalTitle">_0255448</span>
<span class="globalTitleName">VARIABLE GEAR RATIO STEERING SYSTEM</span>
<span class="globalCategory">C</span>
</div>
<h1>VARIABLE GEAR RATIO STEERING&nbsp;&nbsp;VARIABLE GEAR RATIO STEERING SYSTEM&nbsp;&nbsp;C15A4/64&nbsp;&nbsp;Motor Rotation Angle Malfunction&nbsp;&nbsp;C15A9/66&nbsp;&nbsp;Lock Holder Deviation Detection&nbsp;&nbsp;C15AA/66&nbsp;&nbsp;Lock Mechanism Release Incomplete&nbsp;&nbsp;C15AB/66&nbsp;&nbsp;Lock Mechanism Insertion&nbsp;&nbsp;</h1>
<br>
<div id="RM100000000V1D3_04" class="category no03">
<h2>DESCRIPTION</h2>
<div class="content5">
<p>When the VGRS system is normal, the steering actuator assembly conducts current from the VGRS ECU (steering control ECU) to the steering actuator solenoid to release the lock mechanism, enabling motor operation.
</p>
<p>The steering actuator assembly does not operate under any of the following conditions:
</p>
<div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>The engine switch is off.
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>The motor in the actuator is locked by the lock mechanism to prevent rotation (fail-safe function).
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>The system is being protected from overheating (fail-safe function).
</p>
</div>
</div>
</div>
<br>
<p>If the VGRS ECU (steering control ECU) detects a malfunction in the steering actuator lock mechanism or a motor rotation angle error, it stores these DTCs.
</p>
<table summary="">
<colgroup>
<col style="width:20%">
<col style="width:45%">
<col style="width:34%">
</colgroup>
<thead>
<tr>
<th class="alcenter">DTC No.
</th>
<th class="alcenter">DTC Detection Condition
</th>
<th class="alcenter">Trouble Area
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alcenter">C15A4/64
</td>
<td>After starting the engine, the "Actuator Target Angle" and "Actuator Angle" have a difference of 0.5 degrees or more for 1 second or more.
</td>
<td>Steering actuator assembly
</td>
</tr>
<tr>
<td class="alcenter">C15A9/66
</td>
<td>After the engine switch is turned off, the lock pin is inserted with the steering wheel near the neutral position (within 90 degrees of 0), the "Actuator Angle" and "Deep Groove Electrical Angle Position" are at 40 degrees or more.
</td>
<td>Steering actuator assembly
</td>
</tr>
<tr>
<td class="alcenter">C15AA/66
</td>
<td>After starting the engine, the "Motor q Axis Target Voltage" is 1.8 V or higher and 15 seconds or more elapse, or the "Motor q Axis Target Voltage" is 0.6 V or higher and 180 seconds or more elapse.
</td>
<td>Steering actuator assembly
</td>
</tr>
<tr>
<td class="alcenter">C15AB/66
</td>
<td>After the engine switch is turned on (IG), when the motor is operating to confirm the lock pin insertion, the "Motor q Axis Target Voltage" is below 2 V.
</td>
<td>Steering actuator assembly
</td>
</tr>
</tbody>
</table>
<br>
</div>
</div>
<div id="RM100000000V1D3_05" class="category no10">
<h2>CAUTION / NOTICE / HINT</h2>
<div class="content5">
<dl class="atten3">
<dt class="atten3">NOTICE:</dt>
<dd class="atten3">
<p>When replacing the steering actuator assembly, perform actuator angle neutral point calibration and initialization after replacement (See page <a class="mlink" href="javascript:void(0)">Steering&gt;VARIABLE GEAR RATIO STEERING&gt;VARIABLE GEAR RATIO STEERING SYSTEM&gt;CALIBRATION<span class="invisible">201508,999999,_55,_052825,_0255448,RM100000000V1C7,</span></a>).
</p>
</dd>
</dl>
<br>
</div>
</div>
<div id="RM100000000V1D3_06" class="category no01">
<h2>PROCEDURE</h2>
<div class="testgrp" id="RM100000000V1D3_06_0003">
<div class="testtitle"><span class="titleText">1.REPLACE STEERING ACTUATOR ASSEMBLY</span></div>
<div class="content6">
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Replace the steering actuator assembly (See page <a class="mlink" href="javascript:void(0)">Steering&gt;VARIABLE GEAR RATIO STEERING&gt;STEERING ACTUATOR&gt;REMOVAL<span class="invisible">201508,999999,_55,_052825,_0255449,RM100000000V1DG,</span></a>).
</p>
</div>
</div>
</div>
<br>
</div>
<br>
<div class="judgeValueEnd"><p>NEXT
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">END</span></div>
<div class="content6">
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="floatClear"></div>
</div>
</div>
</div>
</div>
<div id="footer"></div>
</div>
</div>
</body>
</html>
