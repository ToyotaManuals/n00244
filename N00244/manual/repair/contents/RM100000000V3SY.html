<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<link rel="stylesheet" type="text/css" href="../../../system/css/global.css">
<link rel="stylesheet" type="text/css" href="../css/contents.css">
<link rel="stylesheet" type="text/css" href="../../../system/css/facebox.css">
<link rel="stylesheet" type="text/css" href="../css/print.css" id="print_css">
<script type="text/javascript" src="../../../system/js/util.js?cntl=Contents" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/dict_const.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/dict_message.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/use.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/contents.js" charset="UTF-8"></script>
<title>Toyota Service Information</title>
</head>
<body>
<div id="wrapper">
<div id="header"></div>
<div id="body">
<div id="contents">
<div id="contentsBody" class="fontEn">
<div class="globalInfo">
<span class="globalPubNo">RM2660E</span>
<span class="globalServcat">_58</span>
<span class="globalServcatName">Vehicle Interior</span>
<span class="globalSection">_052847</span>
<span class="globalSectionName">HEATING / AIR CONDITIONING</span>
<span class="globalTitle">_0255699</span>
<span class="globalTitleName">AIR CONDITIONING SYSTEM(for Automatic Air Conditioning System)</span>
<span class="globalCategory">C</span>
</div>
<h1>HEATING / AIR CONDITIONING&nbsp;&nbsp;AIR CONDITIONING SYSTEM(for Automatic Air Conditioning System)&nbsp;&nbsp;B1488/88&nbsp;&nbsp;Rear Air Mix Damper Control Servo Motor Circuit on Front Passenger Side&nbsp;&nbsp;</h1>
<br>
<div id="RM100000000V3SY_01" class="category no03">
<h2>DESCRIPTION</h2>
<div class="content5">
<p>The rear air mix damper servo motor RH*1 or LH*2 sends pulse signals to indicate the damper position to the air conditioning amplifier assembly. The air conditioning amplifier assembly activates the motor (normal or reverse) based on these signals to move the rear air mix damper to the appropriate position. This adjusts the amount of air passing through the heater core after passing the evaporator and controls the temperature of the blown air.
</p>
<dl class="atten4">
<dt class="atten4">HINT:</dt>
<dd class="atten4">
<p>Confirm that no mechanical problem is present because this trouble code can be output when either a damper link or damper is mechanically locked.
</p>
</dd>
</dl>
<br>
<table summary="">
<colgroup>
<col style="width:33%">
<col style="width:33%">
<col style="width:33%">
</colgroup>
<thead>
<tr>
<th class="alcenter">DTC Code
</th>
<th class="alcenter">DTC Detection Condition
</th>
<th class="alcenter">Trouble Area
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alcenter">B1488/88
</td>
<td class="alleft">The rear air mix damper position does not change even if the air conditioning amplifier assembly operates the air mix damper servo motor.
</td>
<td class="alleft"><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Rear air mix damper servo motor RH*1
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Rear air mix damper servo motor LH*2
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>No. 2 air conditioning harness assembly
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Harness or connector
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Air conditioning amplifier assembly
</p>
</div>
</div>
</div>
<br>
</td>
</tr>
</tbody>
</table>
<br>
<dl class="atten4">
<dt class="atten4">HINT:</dt>
<dd class="atten4">
<p>*1: for LHD
</p>
</dd>
<dd class="atten4">
<p>*2: for RHD
</p>
</dd>
</dl>
<br>
</div>
</div>
<div id="RM100000000V3SY_02" class="category no03">
<h2>WIRING DIAGRAM</h2>
<div class="content5">
<div class="figure">
<div class="cPattern">
<div class="graphic">
<img src="../img/png/E156903E05.png" alt="E156903E05" title="E156903E05">
<div class="indicateinfo">
<span class="caption">
<span class="points">1.39,0.458 1.837,0.600</span>
<span class="captionSize">0.448,0.142</span>
<span class="fontsize">10</span>
<span class="captionText">SiG. A</span>
</span>
<span class="caption">
<span class="points">1.39,0.755 1.837,0.897</span>
<span class="captionSize">0.448,0.142</span>
<span class="fontsize">10</span>
<span class="captionText">SiG. B</span>
</span>
<span class="caption">
<span class="points">1.441,1.124 1.799,1.266</span>
<span class="captionSize">0.357,0.142</span>
<span class="fontsize">10</span>
<span class="captionText">GND</span>
</span>
<span class="caption">
<span class="points">2.414,0.312 2.543,0.468</span>
<span class="captionSize">0.128,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">2</span>
</span>
<span class="caption">
<span class="points">2.414,0.613 2.543,0.769</span>
<span class="captionSize">0.128,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">3</span>
</span>
<span class="caption">
<span class="points">2.414,0.94 2.543,1.096</span>
<span class="captionSize">0.128,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">1</span>
</span>
<span class="caption">
<span class="points">2.414,1.258 2.543,1.414</span>
<span class="captionSize">0.128,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">4</span>
</span>
<span class="caption">
<span class="points">2.414,1.573 2.543,1.729</span>
<span class="captionSize">0.128,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">5</span>
</span>
<span class="caption">
<span class="points">2.743,0.312 2.871,0.468</span>
<span class="captionSize">0.128,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">2</span>
</span>
<span class="caption">
<span class="points">2.743,0.613 2.871,0.769</span>
<span class="captionSize">0.128,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">3</span>
</span>
<span class="caption">
<span class="points">2.743,0.94 2.871,1.096</span>
<span class="captionSize">0.128,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">1</span>
</span>
<span class="caption">
<span class="points">2.743,1.258 2.871,1.414</span>
<span class="captionSize">0.128,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">4</span>
</span>
<span class="caption">
<span class="points">2.743,1.573 2.871,1.729</span>
<span class="captionSize">0.128,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">5</span>
</span>
<span class="caption">
<span class="points">5.306,0.213 5.483,0.368</span>
<span class="captionSize">0.176,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">14</span>
</span>
<span class="caption">
<span class="points">5.306,0.841 5.483,0.997</span>
<span class="captionSize">0.177,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">13</span>
</span>
<span class="caption">
<span class="points">5.306,1.474 5.497,1.630</span>
<span class="captionSize">0.190,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">12</span>
</span>
<span class="caption">
<span class="points">5.011,0.213 5.130,0.368</span>
<span class="captionSize">0.119,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">4</span>
</span>
<span class="caption">
<span class="points">5.001,0.841 5.129,0.997</span>
<span class="captionSize">0.128,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">3</span>
</span>
<span class="caption">
<span class="points">5.001,1.474 5.129,1.630</span>
<span class="captionSize">0.128,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">2</span>
</span>
<span class="caption">
<span class="points">3.194,0.95 3.392,1.092</span>
<span class="captionSize">0.198,0.142</span>
<span class="fontsize">10</span>
<span class="captionText">IC</span>
</span>
<span class="caption">
<span class="points">0.84,1.945 2.667,2.267</span>
<span class="captionSize">1.827,0.323</span>
<span class="fontsize">10</span>
<span class="captionText">Rear Air Mix Damper Servo Motor RH*1 or LH*2</span>
</span>
<span class="caption">
<span class="points">5.503,2.104 7.147,2.461</span>
<span class="captionSize">1.645,0.357</span>
<span class="fontsize">10</span>
<span class="captionText">Air Conditioning Amplifier Assembly</span>
</span>
<span class="caption">
<span class="points">5.534,1.927 5.828,2.083</span>
<span class="captionSize">0.295,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">E36</span>
</span>
<span class="caption">
<span class="points">3.077,2.07 3.372,2.226</span>
<span class="captionSize">0.295,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">K19</span>
</span>
<span class="caption">
<span class="points">3.074,2.255 5.009,2.618</span>
<span class="captionSize">1.936,0.364</span>
<span class="fontsize">10</span>
<span class="captionText">No. 2 Air Conditioning Harness Assembly</span>
</span>
<span class="caption">
<span class="points">4.476,0.315 4.921,0.471</span>
<span class="captionSize">0.445,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">RBBU</span>
</span>
<span class="caption">
<span class="points">4.476,0.945 4.921,1.101</span>
<span class="captionSize">0.445,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">RBUS</span>
</span>
<span class="caption">
<span class="points">4.476,1.59 4.921,1.746</span>
<span class="captionSize">0.445,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">RBUG</span>
</span>
<span class="caption">
<span class="points">5.576,0.315 6.021,0.471</span>
<span class="captionSize">0.445,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">RBBU</span>
</span>
<span class="caption">
<span class="points">5.576,0.945 6.021,1.101</span>
<span class="captionSize">0.445,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">RBUS</span>
</span>
<span class="caption">
<span class="points">5.576,1.59 6.021,1.746</span>
<span class="captionSize">0.445,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">RBUG</span>
</span>
<span class="caption">
<span class="points">0.05,2.24 0.775,2.400</span>
<span class="captionSize">0.725,0.161</span>
<span class="fontsize">10</span>
<span class="captionText">*1: for LHD</span>
</span>
<span class="caption">
<span class="points">0.05,2.51 0.817,2.671</span>
<span class="captionSize">0.767,0.161</span>
<span class="fontsize">10</span>
<span class="captionText">*2: for RHD</span>
</span>
</div>
</div>
</div>
</div>
<br>
</div>
</div>
<div id="RM100000000V3SY_05" class="category no01">
<h2>PROCEDURE</h2>
<div class="testgrp" id="RM100000000V3SY_05_0001">
<div class="testtitle"><span class="titleText">1.READ VALUE USING INTELLIGENT TESTER (REAR AIR MIX DAMPER SERVO MOTOR)</span></div>
<div class="content6">
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Use the Data List to check if the servo motor is functioning properly.
</p>
<table summary="">
<caption>Air Conditioner</caption>
<colgroup>
<col style="width:25%">
<col style="width:25%">
<col style="width:28%">
<col style="width:21%">
</colgroup>
<thead>
<tr>
<th class="alcenter">Tester Display
</th>
<th class="alcenter">Measurement Item/Range
</th>
<th class="alcenter">Normal Condition
</th>
<th class="alcenter">Diagnostic Note
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alcenter">A/M Servo Targ Pulse (Rear P)
</td>
<td class="alleft">Rear air mix damper servo motor (for front passenger side) target pulse /
<br>
Min.: 8, Max.: 53 (for LHD)
<br>
Min.: 9, Max.: 54 (for RHD)
</td>
<td class="alleft">for LHD
<br>
<div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>MAX COOL: 53 (pulse)
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>MAX HOT: 8 (pulse)
</p>
</div>
</div>
</div>
<br>
for RHD
<br>
<div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>MAX COOL: 9 (pulse)
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>MAX HOT: 54 (pulse)
</p>
</div>
</div>
</div>
<br>
</td>
<td class="alcenter">-
</td>
</tr>
</tbody>
</table>
<br>
<dl class="spec">
<dt class="spec">OK:</dt>
<dd class="spec"><p>The display is as specified in the normal condition.
</p>
</dd>
</dl>
<br>
<table summary="">
<caption>Result</caption>
<colgroup>
<col style="width:50%">
<col style="width:49%">
</colgroup>
<thead>
<tr>
<th class="alcenter">Result
</th>
<th class="alcenter">Proceed to
</th>
</tr>
</thead>
<tbody>
<tr>
<td>OK (Checking from the DTC)
</td>
<td class="alcenter">A
</td>
</tr>
<tr>
<td>OK (Checking from the PROBLEM SYMPTOMS TABLE)
</td>
<td class="alcenter">B
</td>
</tr>
<tr>
<td>NG
</td>
<td class="alcenter">C
</td>
</tr>
</tbody>
</table>
<br>
</div>
</div>
</div>
<br>
</div>
<br>
<div class="judgeValueNext"><p>A
</p></div>
<div class="nextAction"><span class="titleText">2.CHECK HARNESS AND CONNECTOR (NO. 2 AIR CONDITIONING HARNESS - AIR CONDITIONING AMPLIFIER)</span></div>
<div class="judgeValueEnd"><p>B
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE</span></div>
<div class="content6">
<p>(See page<a class="mlink" href="javascript:void(0)">Vehicle Interior&gt;HEATING / AIR CONDITIONING&gt;AIR CONDITIONING SYSTEM (for Automatic Air Conditioning System)&gt;PROBLEM SYMPTOMS TABLE<span class="invisible">201408,201508,_58,_052847,_0255699,RM100000000V3QO,</span></a>)
</p>
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="judgeValueEnd"><p>C
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REPLACE AIR CONDITIONING AMPLIFIER ASSEMBLY</span></div>
<div class="content6">
<p>(See page<a class="mlink" href="javascript:void(0)">Vehicle Interior&gt;HEATING / AIR CONDITIONING&gt;AIR CONDITIONING AMPLIFIER&gt;REMOVAL<span class="invisible">201408,201508,_58,_052847,_0255673,RM100000000V3MW,</span></a>)
</p>
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="floatClear"></div>
</div>
<div class="testgrp" id="RM100000000V3SY_05_0002">
<div class="testtitle"><span class="titleText">2.CHECK HARNESS AND CONNECTOR (NO. 2 AIR CONDITIONING HARNESS - AIR CONDITIONING AMPLIFIER)</span></div>
<div class="content6">
<div class="figure">
<div class="cPattern">
<div class="graphic">
<img src="../img/png/E349691E03.png" alt="E349691E03" title="E349691E03">
<div class="indicateinfo">
<span class="rectangle">
<span class="points">0.646,1.813 1.031,2.063</span>
<span class="rectangleSize">0.385,0.25</span>
</span>
<span class="rectangle">
<span class="points">5.146,1.125 5.531,1.375</span>
<span class="rectangleSize">0.385,0.25</span>
</span>
<span class="line">
<span class="points">1.604,2.01 2.188,1.719</span>
<span class="points">2.188,1.719 2.281,1.719</span>
<span class="whiteedge">false</span>
</span>
<span class="line">
<span class="points">1.729,2.021 2.563,2.021</span>
<span class="whiteedge">false</span>
</span>
<span class="line">
<span class="points">1.885,2.094 2.177,2.333</span>
<span class="points">2.177,2.333 2.281,2.333</span>
<span class="whiteedge">false</span>
</span>
<span class="line">
<span class="points">4.531,3 4.688,3</span>
<span class="points">4.688,3 5.313,2.604</span>
<span class="whiteedge">false</span>
</span>
<span class="line">
<span class="points">4.781,3.292 4.938,3.292</span>
<span class="points">4.938,3.292 5.406,2.646</span>
<span class="whiteedge">false</span>
</span>
<span class="line">
<span class="points">6.146,3.292 6.01,3.292</span>
<span class="points">6.01,3.292 5.49,2.646</span>
<span class="whiteedge">false</span>
</span>
<span class="caption">
<span class="points">0.177,0.156 2.76,0.573</span>
<span class="captionSize">2.583,0.417</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">Front view of wire harness connector: (to No. 2 Air Conditioning Harness Assembly)</span>
</span>
<span class="caption">
<span class="points">4.063,0.125 6.563,0.542</span>
<span class="captionSize">2.5,0.417</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">Rear view of wire harness connector: (to Air Conditioning Amplifier Assembly)</span>
</span>
<span class="caption">
<span class="points">0.708,1.854 1.052,2.052</span>
<span class="captionSize">0.344,0.198</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">K19</span>
</span>
<span class="caption">
<span class="points">2.656,1.906 3.177,2.104</span>
<span class="captionSize">0.521,0.198</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">RBUS</span>
</span>
<span class="caption">
<span class="points">2.354,2.25 2.875,2.448</span>
<span class="captionSize">0.521,0.198</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">RBBU</span>
</span>
<span class="caption">
<span class="points">5.229,1.156 5.521,1.354</span>
<span class="captionSize">0.292,0.198</span>
<span class="fontsize">10</span>
<span class="filled">true</span>
<span class="captionText">E36</span>
</span>
<span class="caption">
<span class="points">4.094,2.917 4.615,3.115</span>
<span class="captionSize">0.521,0.198</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">RBBU</span>
</span>
<span class="caption">
<span class="points">4.333,3.208 4.854,3.406</span>
<span class="captionSize">0.521,0.198</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">RBUS</span>
</span>
<span class="caption">
<span class="points">2.344,1.635 2.865,1.833</span>
<span class="captionSize">0.521,0.198</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">RBUG</span>
</span>
<span class="caption">
<span class="points">6.198,3.198 6.719,3.396</span>
<span class="captionSize">0.521,0.198</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">RBUG</span>
</span>
</div>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Disconnect the K19 No. 2 air conditioning harness connector.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">b.</div>
<div class="test1Body">
<p>Disconnect the E36 amplifier connector.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">c.</div>
<div class="test1Body">
<p>Measure the resistance according to the value(s) in the table below.
</p>
<dl class="spec">
<dt class="spec">Standard Resistance:</dt>
<dd class="spec"><table summary="" class="half">
<colgroup>
<col style="width:33%">
<col style="width:33%">
<col style="width:33%">
</colgroup>
<thead>
<tr>
<th class="alcenter">Tester Connection
</th>
<th class="alcenter">Condition
</th>
<th class="alcenter">Specified Condition
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alcenter">K19-2 (RBUG) - E36-12 (RBUG)
</td>
<td class="alcenter" rowspan="3">Always
</td>
<td class="alcenter" rowspan="3">Below 1 Ω
</td>
</tr>
<tr>
<td class="alcenter">K19-3 (RBUS) - E36-13 (RBUS)
</td>
</tr>
<tr>
<td class="alcenter">K19-4 (RBBU) - E36-14 (RBBU)
</td>
</tr>
<tr>
<td class="alcenter">E36-12 (RBUG) - Body ground
</td>
<td class="alcenter" rowspan="3">Always
</td>
<td class="alcenter" rowspan="3">10 kΩ or higher
</td>
</tr>
<tr>
<td class="alcenter">E36-13 (RBUS) - Body ground
</td>
</tr>
<tr>
<td class="alcenter">E36-14 (RBBU) - Body ground
</td>
</tr>
</tbody>
</table>
<br>
</dd>
</dl>
<br>
</div>
</div>
</div>
<br>
</div>
<br>
<div class="judgeValueNext"><p>OK
</p></div>
<div class="nextAction"><span class="titleText">3.CHECK REAR AIR MIX DAMPER SERVO MOTOR</span></div>
<div class="judgeValueEnd"><p>NG
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REPAIR OR REPLACE HARNESS OR CONNECTOR</span></div>
<div class="content6">
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="floatClear"></div>
</div>
<div class="testgrp" id="RM100000000V3SY_05_0003">
<div class="testtitle"><span class="titleText">3.CHECK REAR AIR MIX DAMPER SERVO MOTOR</span></div>
<div class="content6">
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Replace the rear air mix damper servo motor (See page <a class="mlink" href="javascript:void(0)">Vehicle Interior&gt;HEATING / AIR CONDITIONING&gt;REAR AIR CONDITIONING UNIT (w/ Heater)&gt;DISASSEMBLY<span class="invisible">201408,999999,_58,_052847,_0255670,RM100000000V3MD,</span></a>).
</p>
<dl class="atten4">
<dt class="atten4">HINT:</dt>
<dd class="atten4">
<p>Since the servo motor cannot be inspected while it is removed from the vehicle, replace the servo motor with a normal one.
</p>
</dd>
</dl>
<br>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">b.</div>
<div class="test1Body">
<p>Clear the DTCs (See page <a class="mlink" href="javascript:void(0)">Vehicle Interior&gt;HEATING / AIR CONDITIONING&gt;AIR CONDITIONING SYSTEM (for Automatic Air Conditioning System)&gt;DTC CHECK / CLEAR<span class="invisible">201408,999999,_58,_052847,_0255699,RM100000000V3RR,</span></a>).
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">c.</div>
<div class="test1Body">
<p>Check for DTCs (See page <a class="mlink" href="javascript:void(0)">Vehicle Interior&gt;HEATING / AIR CONDITIONING&gt;AIR CONDITIONING SYSTEM (for Automatic Air Conditioning System)&gt;DTC CHECK / CLEAR<span class="invisible">201408,999999,_58,_052847,_0255699,RM100000000V3RR,</span></a>).
</p>
<table summary="" class="half">
<caption>Result</caption>
<colgroup>
<col style="width:50%">
<col style="width:49%">
</colgroup>
<thead>
<tr>
<th class="alcenter">Result
</th>
<th class="alcenter">Proceed to
</th>
</tr>
</thead>
<tbody>
<tr>
<td>DTC B1488/88 is not output
</td>
<td class="alcenter">A
</td>
</tr>
<tr>
<td>DTC B1488/88 is output
</td>
<td class="alcenter">B
</td>
</tr>
</tbody>
</table>
<br>
</div>
</div>
</div>
<br>
</div>
<br>
<div class="judgeValueEnd"><p>A
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">END (REAR AIR MIX DAMPER SERVO MOTOR IS DEFECTIVE)</span></div>
<div class="content6">
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="judgeValueNext"><p>B
</p></div>
<div class="nextAction"><span class="titleText">4.CHECK NO. 2 AIR CONDITIONING HARNESS ASSEMBLY</span></div>
<div class="floatClear"></div>
</div>
<div class="testgrp" id="RM100000000V3SY_05_0009">
<div class="testtitle"><span class="titleText">4.CHECK NO. 2 AIR CONDITIONING HARNESS ASSEMBLY</span></div>
<div class="content6">
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Replace the No. 2 air conditioning harness assembly (See page <a class="mlink" href="javascript:void(0)">Vehicle Interior&gt;HEATING / AIR CONDITIONING&gt;REAR AIR CONDITIONING UNIT (w/ Heater)&gt;DISASSEMBLY<span class="invisible">201408,999999,_58,_052847,_0255670,RM100000000V3MD,</span></a>).
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">b.</div>
<div class="test1Body">
<p>Clear the DTCs (See page <a class="mlink" href="javascript:void(0)">Vehicle Interior&gt;HEATING / AIR CONDITIONING&gt;AIR CONDITIONING SYSTEM (for Automatic Air Conditioning System)&gt;DTC CHECK / CLEAR<span class="invisible">201408,999999,_58,_052847,_0255699,RM100000000V3RR,</span></a>).
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">c.</div>
<div class="test1Body">
<p>Check for DTCs (See page <a class="mlink" href="javascript:void(0)">Vehicle Interior&gt;HEATING / AIR CONDITIONING&gt;AIR CONDITIONING SYSTEM (for Automatic Air Conditioning System)&gt;DTC CHECK / CLEAR<span class="invisible">201408,999999,_58,_052847,_0255699,RM100000000V3RR,</span></a>).
</p>
<table summary="" class="half">
<caption>Result</caption>
<colgroup>
<col style="width:50%">
<col style="width:49%">
</colgroup>
<thead>
<tr>
<th class="alcenter">Result
</th>
<th class="alcenter">Proceed to
</th>
</tr>
</thead>
<tbody>
<tr>
<td>DTC B1488/88 is not output
</td>
<td class="alcenter">A
</td>
</tr>
<tr>
<td>DTC B1488/88 is output
</td>
<td class="alcenter">B
</td>
</tr>
</tbody>
</table>
<br>
</div>
</div>
</div>
<br>
</div>
<br>
<div class="judgeValueEnd"><p>A
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">END (NO. 2 AIR CONDITIONING HARNESS ASSEMBLY IS DEFECTIVE)</span></div>
<div class="content6">
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="judgeValueEnd"><p>B
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REPLACE AIR CONDITIONING AMPLIFIER ASSEMBLY</span></div>
<div class="content6">
<p>(See page<a class="mlink" href="javascript:void(0)">Vehicle Interior&gt;HEATING / AIR CONDITIONING&gt;AIR CONDITIONING AMPLIFIER&gt;REMOVAL<span class="invisible">201408,201508,_58,_052847,_0255673,RM100000000V3MW,</span></a>)
</p>
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="floatClear"></div>
</div>
</div>
</div>
</div>
<div id="footer"></div>
</div>
</div>
</body>
</html>
