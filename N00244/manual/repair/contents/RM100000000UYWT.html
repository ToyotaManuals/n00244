<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<link rel="stylesheet" type="text/css" href="../../../system/css/global.css">
<link rel="stylesheet" type="text/css" href="../css/contents.css">
<link rel="stylesheet" type="text/css" href="../../../system/css/facebox.css">
<link rel="stylesheet" type="text/css" href="../css/print.css" id="print_css">
<script type="text/javascript" src="../../../system/js/util.js?cntl=Contents" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/dict_const.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/dict_message.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/use.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/contents.js" charset="UTF-8"></script>
<title>Toyota Service Information</title>
</head>
<body>
<div id="wrapper">
<div id="header"></div>
<div id="body">
<div id="contents">
<div id="contentsBody" class="fontEn">
<div class="globalInfo">
<span class="globalPubNo">RM2660E</span>
<span class="globalServcat">_51</span>
<span class="globalServcatName">Engine / Hybrid System</span>
<span class="globalSection">_052775</span>
<span class="globalSectionName">3UR-FE ENGINE CONTROL</span>
<span class="globalTitle">_0255032</span>
<span class="globalTitleName">SFI SYSTEM</span>
<span class="globalCategory">C</span>
</div>
<h1>3UR-FE ENGINE CONTROL&nbsp;&nbsp;SFI SYSTEM&nbsp;&nbsp;P0116&nbsp;&nbsp;Engine Coolant Temperature Circuit Range / Performance Problem&nbsp;&nbsp;</h1>
<br>
<div id="RM100000000UYWT_01" class="category no03">
<h2>DESCRIPTION</h2>
<div class="content5">
<p>Refer to DTC P0115 (See page <a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;3UR-FE (ENGINE CONTROL)&gt;SFI SYSTEM(w/o Secondary Air Injection System)&gt;P0115<span class="invisible">201408,201508,_51,_052775,_0255032,RM100000000UYUW_01,P0115</span></a>).
</p>
<table summary="">
<colgroup>
<col style="width:12%">
<col style="width:44%">
<col style="width:43%">
</colgroup>
<thead>
<tr>
<th class="alcenter">DTC Code
</th>
<th class="alcenter">DTC Detection Condition
</th>
<th class="alcenter">Trouble Area
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alcenter">P0116
</td>
<td>The Engine Coolant Temperature (ECT) is between 35&deg;C and 60&deg;C (95&deg;F and 140&deg;F) when the engine is started, and conditions (a) and (b) are met (2 trip detection logic):
<br>
<div class="list1">
<div class="list1Item">
<div class="list1Head"></div>
<div class="list1Body"><p>(a) The vehicle is driven at varying speeds (accelerated and decelerated).
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head"></div>
<div class="list1Body"><p>(b) The ECT remains within 3&deg;C (5.4&deg;F) of the initial ECT.
</p>
</div>
</div>
</div>
<br>
</td>
<td><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Water inlet sub-assembly with thermostat
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>ECT sensor
</p>
</div>
</div>
</div>
<br>
</td>
</tr>
</tbody>
</table>
<br>
</div>
</div>
<div id="RM100000000UYWT_02" class="category no03">
<h2>MONITOR DESCRIPTION</h2>
<div class="content5">
<p>The ECT sensor is used to monitor the ECT. The ECT sensor has a built-in thermistor with a resistance that varies according to the temperature of the engine coolant. When the ECT is low, the resistance of the thermistor increases. When the temperature is high, the resistance drops. These variations in the resistance are reflected in the output voltage from the ECT sensor.
</p>
<p>The ECM monitors the sensor voltage and uses this value to calculate the ECT. If the sensor output voltage deviates from the normal operating range, the ECM interprets this deviation as a malfunction in the ECT sensor and stores the DTC.
</p>
<p>Example:
</p>
<p>Upon starting the engine, the ECT is between 35&deg;C and 60&deg;C (95&deg;F and 140&deg;F). If after driving for 250 seconds, the ECT remains within 3&deg;C (5.4&deg;F) of the starting temperature, the DTC is stored (2 trip detection logic).
</p>
</div>
</div>
<div id="RM100000000UYWT_07" class="category no10">
<h2>CAUTION / NOTICE / HINT</h2>
<div class="content5">
<dl class="atten4">
<dt class="atten4">HINT:</dt>
<dd class="atten4"><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>If any of DTCs P0115, P0117, P0118 or P0125 is output simultaneously with DTC P0116, the ECT sensor may have an open or a short circuit. Troubleshoot those DTCs first.
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.
</p>
</div>
</div>
</div>
<br>
</dd>
</dl>
<br>
</div>
</div>
<div id="RM100000000UYWT_08" class="category no01">
<h2>PROCEDURE</h2>
<div class="testgrp" id="RM100000000UYWT_08_0001">
<div class="testtitle"><span class="titleText">1.CHECK ANY OTHER DTCS OUTPUT (IN ADDITION TO DTC P0116)</span></div>
<div class="content6">
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Connect the intelligent tester to the DLC3.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">b.</div>
<div class="test1Body">
<p>Turn the engine switch on (IG).
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">c.</div>
<div class="test1Body">
<p>Turn the tester on.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">d.</div>
<div class="test1Body">
<p>Enter the following menus: Powertrain / Engine and ECT / DTC.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">e.</div>
<div class="test1Body">
<p>Read the DTC.
</p>
<table summary="">
<caption>Result</caption>
<colgroup>
<col style="width:50%">
<col style="width:49%">
</colgroup>
<thead>
<tr>
<th>Result
</th>
<th>Proceed to
</th>
</tr>
</thead>
<tbody>
<tr>
<td>P0116 is output
</td>
<td>A
</td>
</tr>
<tr>
<td>P0116 and other DTCs are output
</td>
<td>B
</td>
</tr>
</tbody>
</table>
<br>
</div>
</div>
</div>
<br>
</div>
<br>
<div class="judgeValueNext"><p>A
</p></div>
<div class="nextAction"><span class="titleText">2.INSPECT WATER INLET SUB-ASSEMBLY WITH THERMOSTAT</span></div>
<div class="judgeValueEnd"><p>B
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">GO TO DTC CHART</span></div>
<div class="content6">
<p>(See page<a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;3UR-FE (ENGINE CONTROL)&gt;SFI SYSTEM(w/o Secondary Air Injection System)&gt;DIAGNOSTIC TROUBLE CODE CHART<span class="invisible">201408,201508,_51,_052775,_0255032,RM100000000UYUL,</span></a>)
</p>
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="floatClear"></div>
</div>
<div class="testgrp" id="RM100000000UYWT_08_0006">
<div class="testtitle"><span class="titleText">2.INSPECT WATER INLET SUB-ASSEMBLY WITH THERMOSTAT</span></div>
<div class="content6">
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Remove the water inlet sub-assembly with thermostat (See page <a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;3UR-FE (COOLING)&gt;THERMOSTAT&gt;REMOVAL<span class="invisible">201408,999999,_51,_052793,_0255190,RM100000000UZJ8,</span></a>).
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">b.</div>
<div class="test1Body">
<p>Measure the valve opening temperature of the water inlet sub-assembly with thermostat.
</p>
<dl class="spec">
<dt class="spec">Standard opening temperature:</dt>
<dd class="spec"><p>80 to 84&deg;C (176 to 183&deg;F)
</p>
</dd>
</dl>
<br>
<dl class="atten4">
<dt class="atten4">HINT:</dt>
<dd class="atten4">
<p>In addition to the above check, confirm that the valve is completely closed when the temperature is below the standard.
</p>
</dd>
</dl>
<br>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">c.</div>
<div class="test1Body">
<p>Reinstall the water inlet sub-assembly with thermostat (See page <a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;3UR-FE (COOLING)&gt;THERMOSTAT&gt;INSTALLATION<span class="invisible">201408,999999,_51,_052793,_0255190,RM100000000UZJ5,</span></a>).
</p>
</div>
</div>
</div>
<br>
</div>
<br>
<div class="judgeValueEnd"><p>OK
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REPLACE ENGINE COOLANT TEMPERATURE SENSOR</span></div>
<div class="content6">
<p>(See page<a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;3UR-FE (ENGINE CONTROL)&gt;ENGINE COOLANT TEMPERATURE SENSOR&gt;REMOVAL<span class="invisible">201408,999999,_51,_052775,_0255026,RM100000000UYTV,</span></a>)
</p>
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="judgeValueEnd"><p>NG
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REPLACE WATER INLET SUB-ASSEMBLY WITH THERMOSTAT</span></div>
<div class="content6">
<p>(See page<a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;3UR-FE (COOLING)&gt;THERMOSTAT&gt;REMOVAL<span class="invisible">201408,999999,_51,_052793,_0255190,RM100000000UZJ8,</span></a>)
</p>
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="floatClear"></div>
</div>
</div>
</div>
</div>
<div id="footer"></div>
</div>
</div>
</body>
</html>
